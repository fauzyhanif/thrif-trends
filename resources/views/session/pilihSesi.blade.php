@extends('index')

@section('content')
<section class="content-header">
    <h1>
        Pilih Usergroup
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Silahkan pilih Tahun Akademik dan Gelombang</h3>
                </div>
                <div class="box-body">
                    @foreach ($datas as $data)

                    <a href="{{ url('/set-sesi-tahun-akd', [substr($data->thn_akd,0,4), $data->gelombang]) }}" class="btn btn-block btn-social btn-linkedin">
                        <i class='fa fa-calendar'></i> TA {{ $data->thn_akd }} Gelombang {{ $data->gelombang }}
                    </a>
                    @endforeach
                </div>
              </div>
        </div>
    </div>
</section>
@endsection
