@include('layouts.admin.header')
<div class="content-wrapper">
    <div class="container-fluid">
        @yield('content')
    </div>
</div>
@include('layouts.admin.footer')
