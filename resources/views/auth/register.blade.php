<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Thrif Trends | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ url('public/v4/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ url('public/v4/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ url('public/v4/dist/css/adminlte.min.css') }}">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  @include('assets.css.custom')
  <style>
    .custom-section {
      padding: 7em 0;
    }

    .wrapp {
      width: 100%;
      height: 100%;
      overflow: hidden;
      background: #fff;
      border-radius: 10px;
      border: 1px solid gray;
      padding: 30px;
      margin-left: 1px;
    }

    @media only screen and (max-device-width: 480px) {
      .form-login {
        margin-top: 50px !important;
      }
    }
  </style>
</head>
<body>

  <section class="custom-section">
    <div class="container">

      <div class="row justify-content-center wrapp">
        <div class="col-lg-6 col-md-12 m-auto">
          <img src="{{ url('public/img/image_login.png') }}" alt="" class="img-fluid">
        </div>

        <div class="col-lg-6 col-md-12 m-auto form-login">

          <h2 class="font-weight-bold text-center">Registrasi</h2>
          <div class="text-center">
            <img src="{{ url('public/img/logo_new.png') }}" alt="" class="img-thumbnail" style="width: 50%">
          </div>
          
          <p class="login-box-msg mt-3">Silahkan lengkapi form dibawah ini</p>

          @if ($errors->any())
            <div class="alert alert-danger">
              <strong>{{ $errors->first() }}</strong>
            </div>
          @endif

          <form action="{{ url('register') }}" method="post">
            @csrf
            <div class="row">           
              <div class="form-group col-lg-6">
                <label for="">Nama Lengkap</label>
                <input type="text" name="nama" required class="form-control">
              </div>

              <div class="form-group col-lg-6">
                <label for="">No Handphone</label>
                <input type="text" name="no_hp" required class="form-control">
              </div>

              <div class="form-group col-lg-6">
                <label for="">Username</label>
                <input type="text" name="username" required class="form-control">
              </div>

              <div class="form-group col-lg-6">
                <label for="">Email</label>
                <input type="text" name="email" required class="form-control">
              </div>

              <div class="form-group col-lg-12">
                <label for="">Password</label>
                <input type="password" name="password" required class="form-control">
              </div>

              <div class="form-group col-lg-12">
                <label for="">Alamat Lengkap</label>
                <input type="text" name="alamat" required class="form-control">
              </div>

              <div class="form-group col-lg-12">
                <label>Provinsi</label>
                <select name="id_provinsi" class="form-control" required onchange="cariKota(this.value)">
                  <option value="">-- Pilih Provinsi --</option>
                  @foreach ($provinces as $province)
                    <option value="{{ $province->province_id }}">{{ $province->province_name }}</option>
                  @endforeach
                </select>
              </div>

              <div class="form-group col-lg-6">
                <label>Kota</label>
                <select name="id_kota" class="form-control" id="kota" required onchange="cariKecamatan(this.value)">
                  <option value="">-- Pilih Kota/Kabupaten --</option>
                </select>
              </div>

              <div class="form-group col-lg-6">
                <label>Kecamatan</label>
                <select name="id_kecamatan" class="form-control" id="kecamatan" required>
                  <option value="">-- Pilih Kecamatan --</option>
                </select>
              </div>

            </div>

            <button type="submit" class="btn btn-orange btn-block mt-2">Registrasi</button>
          </form>

          <p class="mt-4 text-center">Sudah punya akun? Lakukan <a href="{{ url('auth') }}">Login</a></p>
        </div>
      </div>
    </div>
  </section>

  <script>
    function cariKota(IdProvinsi) {
      var base = {!! json_encode(url('/helpers/cari-kota')) !!};
      var url = base + "/" + IdProvinsi;
      $.ajax({
        type: "GET",
        url: url,
        dataType : 'json',
        success: function(result){
          var y = '<option value="">-- Pili Kota/Kabupaten --</option>'
          for (var x in result){
            y += '<option value="'+result[x].city_id+'">'+result[x].city_name+'</option>'
          }

          $('#kota').html(y)
        }
      });
    }

    function cariKecamatan(IdKota) {
      var base = {!! json_encode(url('/helpers/cari-kecamatan')) !!};
      var url = base + "/" + IdKota;
      $.ajax({
        type: "GET",
        url: url,
        dataType : 'json',
        success: function(result){
          var y = '<option value="">-- Pilih Kecamatan --</option>'
          for (var x in result){
            y += '<option value="'+result[x].subdistrict_id+'">'+result[x].subdistrict_name+'</option>'
          }

          $('#kecamatan').html(y)
        }
      });
    }
  </script>

<!-- jQuery -->
<script src="{{ url('public/v4/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ url('public/v4/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ url('public/v4/dist/js/adminlte.min.js') }}"></script>

</body>
</html>
