<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Thrif Trends - Verifikasi Email</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ url('public/v4/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ url('public/v4/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ url('public/v4/dist/css/adminlte.min.css') }}">
  </head>
  <body class="hold-transition login-page">

    <div class="login-box">
      <div class="login-logo">
        <a href=""><b>Verifikasi Email</b></a>
      </div>
      <div class="card">
        <div class="card-body login-card-body">
          <p class="login-box-msg">Silahkan cek email anda</p>
          <p>Jika tidak ada silahkan klik tombol dibawah</p>

          <a href="{{ url('resend-email-verification?email='.$email) }}" class="btn btn-primary btn-block">Kirim Ulang</a>
        </div>
      </div>
    </div>

    <!-- jQuery -->
    <script src="{{ url('public/v4/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ url('public/v4/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ url('public/v4/dist/js/adminlte.min.js') }}"></script>
  </body>
</html>
