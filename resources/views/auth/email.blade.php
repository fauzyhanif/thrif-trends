<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Thriftrends.com - Verification Mail</title>
</head>
<body>
  <center>{{ $title }}</center>
  Silahkan klik <a href="{{ url('token-verification?email='.$email.'&token='.$token) }}">VERIFIKASI</a> untuk verifikasi email. <br><br>
  Untuk customer yang ingin menjual baju nya, silahkan klik link whatsapp admin berikut ini <a href="https://wa.link/7242kn" target="_blank" rel="noopener noreferrer">https://wa.link/7242kn</a> 


</body>
</html>