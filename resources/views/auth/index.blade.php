<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Thrif Trends | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ url('public/v4/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ url('public/v4/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ url('public/v4/dist/css/adminlte.min.css') }}">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  @include('assets.css.custom')

<style>
    .custom-section {
      padding: 7em 0;
    }

    .wrapp {
      width: 100%;
      height: 100%;
      overflow: hidden;
      background: #fff;
      border-radius: 10px;
      border: 1px solid gray;
      padding: 30px;
      margin-left: 1px;
    }

    @media only screen and (max-device-width: 480px) {
      .form-login {
        margin-top: 50px !important;
      }
    }
  </style>
</head>
<body>

  <section class="custom-section">
    <div class="container">

      <div class="row justify-content-center wrapp">
        <div class="col-lg-6 col-md-12">
          <img src="{{ url('public/img/image_login.png') }}" alt="" class="img-fluid">
        </div>

        <div class="col-lg-6 col-md-12 m-auto form-login text-center">
          
          <h2 class="font-weight-bold text-center">Login</h2>
          <img src="{{ url('public/img/logo_new.png') }}" alt="" class="img-thumbnail text-center" style="width: 50%">
          
          <p class="login-box-msg mt-3">Masukan Username serta Password dengan benar.</p>

          @if ($errors->any())
            <div class="alert alert-danger">
              <strong>{{ $errors->first() }}</strong>
            </div>
          @endif
          
          <form action="{{ url('doAuth') }}" method="post">
            @csrf
            <div class="input-group mb-3">
              <input type="text" name="username" value="{{ old('username') }}" class="form-control" placeholder="Username">
              <div class="input-group-append">
                <div class="input-group-text">
                <span class="fas fa-envelope"></span>
                </div>
              </div>
            </div>

            <div class="input-group mb-3">
              <input type="password" name="password" class="form-control" placeholder="Password">
              <div class="input-group-append">
                <div class="input-group-text">
                <span class="fas fa-lock"></span>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-12">
                <button type="submit" class="btn btn-orange btn-block">Login</button>
              </div>
            </div>
          </form>

          <p class="mt-4 text-center">Belum punya akun? Lakukan <a href="{{ url('register') }}">Registrasi</a></p>
        </div>
      </div>
    </div>
  </section>

<!-- jQuery -->
<script src="{{ url('public/v4/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ url('public/v4/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ url('public/v4/dist/js/adminlte.min.js') }}"></script>

</body>
</html>
