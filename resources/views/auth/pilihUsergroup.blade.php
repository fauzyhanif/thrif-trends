@extends('admin')

@section('content')
<div class="content-header">
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Pilih Usergroup</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Authentication</a></li>
                <li class="breadcrumb-item active">Pilih Usergroup</li>
            </ol>
        </div><!-- /.col -->
    </div><!-- /.row -->
</div>

<section class="content">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="card">
                <div class="card-header with-border">
                    <h3 class="card-title">Silahkan pilih Usergroup</h3>
                </div>
                <div class="card-body">
                    <?php
                        $r = array_filter(explode(',', $sessionUsergroup));
                        foreach ($dtUsergroup as $ug){
                            if (in_array($ug->id_usergroup, $r)){
                    ?>
                        <a href="{{ url('set-usergroup', $ug->id_usergroup) }}" class="btn btn-block bg-gradient-primary">
                            {{ $ug->nama }}
                        </a>
                    <?php }} ?>
                </div>
              </div>
        </div>
    </div>
</section>
@endsection
