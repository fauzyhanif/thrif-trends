<!-- The Modal -->
<div class="modal" id="modal-request-jtr">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Konfirmasi</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                Anda yakin ingin memakai jasa pengiriman JNE servis JTR?
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button>
                <button type="button" class="btn btn-light" onclick="requestJtr()">Ya, Pakai JTR</button>
            </div>

        </div>
    </div>
</div>
