@extends('distributor')

@section('content')
<section class="content-header" style="margin-top: 70px;">
    <h1>
        Request JTR
    </h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mt-2 mb-3 text-center">
                            <div class="col-md-12">
                                <h3 class="text-muted">
                                    Yeaayy.. <br>
                                    Pesanan anda sedang dalam tahap request jasa pengiriman JNE servis JTR.
                                </h3>
                            </div>
                        </div>

                        <div class="row mt-2 mb-3 text-center">
                            <div class="col-md-12">
                                <span class="text-muted">
                                    Request jasa pengiriman JTR behasil diajukan, mohon tunggu 1 x 24 jam dan jangan membayar pesanan sampai Admin
                                    menghubungi Anda melalui Whatsapp untuk memberitahu Total Ongkir dan Total Tagihan yang harus dibayar.
                                </span>
                            </div>

                            <div class="col-md-12">
                                <span class="text-muted">
                                    Terima kasih.
                                </span>
                            </div>
                        </div>

                        <div class="row my-2 text-center">
                            <div class="col-md-12">
                                <span class="text-muted">No Invoice Anda</span>
                                <h2>
                                    <label class="badge badge-light" id="no-invoice">{{ $invoiceId }}</label>
                                    <button class="btn btn-secondary btn-sm" onclick="copyToClipboard('#no-invoice')">
                                        Copy
                                    </button>
                                </h2>
                            </div>
                        </div>


                        <div class="row mt-2 mb-1 text-center">
                            <div class="col-md-12">
                                <a href="{{ url('dist/dashboard') }}" class="text-pink" style="text-decoration: underline">Kembali ke Dashboard</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
}
</script>

@endsection
