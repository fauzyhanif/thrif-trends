@extends('customer')
@section('content')

@php
    $totalWeight = 0;
    $totalItem = 0;
    $totalProduct = 0;
    $totalShippingCost = 0;
    $totalUnit = 0;
@endphp

<section class="content-header" style="margin-top: 70px;">
    <h4>
        <a href="{{ url('customer/keranjang') }}" style="color: grey">
            <i class="fas fa-long-arrow-alt-left"></i>
        </a>
        &nbsp;&nbsp;
        Checkout
    </h4>
</section>

<section class="content">
    <div class="container-fluid">
        <form action="{{ url('/customer/checkout/buat-pesanan') }}" method="POST" data-remote>    
            @csrf    
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title text-secondary"><i class="fas fa-map-marker-alt"></i>&nbsp; Alamat Pengiriman</h3>
                        </div>
                        <div class="card-body default-address" style="display: block">
                            <span class="text-muted">
                                {{ $myProfile->nama . ' (' . $myProfile->no_hp . ')' }}  <br>
                                {{ $myProfile->alamat . ', Kec. ' . $myProfile->subdistrict_name . ', ' . $myProfile->city_name . ', ' . $myProfile->province_name }}
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            @foreach ($units as $unit)
                <input type="hidden" name="units[]" value="{{ $unit->id_unit }}">
                @php
                    $subtotalWeight = 0;
                    $subtotalItem = 0;
                    $subtotalProduct = 0;
                    $subtotalShippingCost = 0;
                    $totalUnit += 1;
                @endphp
                <div class="row mt-2">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title">{{ $unit->nama }}</h5> <br>
                                <span class="text-muted">{{ $unit->city_name }}</span>
                            </div>
                            <div class="card-body">
                                @foreach ($myCart as $item)
                                    @if ($item->id_unit == $unit->id_unit)
                                        <div class="border-bottom">
                                            <div class="row my-3">
                                                <div class="col-md-7">
                                                    <h5 class="font-weight-normal text-dark">{{ $item->nama }}</h5>
                                                </div>
                                                <div class="col-md-3 col-6">
                                                    <span class="font-weight-light text-muted">{{ $item->qty }} x @currency($item->harga)</span>
                                                </div>
                                                <div class="col-md-2 col-6 text-right">
                                                    <span class="font-weight-normal text-dark">@currency($item->jumlah)</span>
                                                </div>
                                            </div>
                                        </div>

                                        @php
                                            $subtotalWeight += ($item->berat * $item->qty);
                                            $subtotalItem += $item->qty;
                                            $subtotalProduct += $item->jumlah;
                                            $subtotalShippingCost = 0;
                                        @endphp
                                    @endif
                                @endforeach

                                <h5 class="mt-3">Jasa Pengiriman</h5>
                                <div class="logistic-list">
                                    @php
                                        $no = 1;
                                        $noForService = 1;
                                    @endphp
                                    @foreach ($costs[$unit->id_unit] as $cost)

                                        <button class="btn btn-default btn-block btn-flat mb-2" type="button"
                                            data-toggle="collapse" data-target="#collapse{{$unit->id_unit}}{{ $no }}" aria-expanded="false"
                                            aria-controls="collapseExample" style="text-align: left">
                                            {{ $cost->name }} <span class="text-muted"></span>
                                        </button>

                                        <div class="collapse" id="collapse{{$unit->id_unit}}{{ $no }}">
                                            <div class="card card-body">
                                                @foreach ($cost->costs as $service)
                                                    <div class="form-group border">
                                                        <div class="custom-control custom-radio my-3 mx-2">
                                                            <input
                                                                class="custom-control-input radio-cost"
                                                                type="radio"
                                                                id="customRadio{{$unit->id_unit}}{{ $noForService }}"
                                                                name="customRadio{{$unit->id_unit}}"
                                                                value="{{ $cost->name . ' * ' . $service->description . ' * ' . $service->cost[0]->etd . ' * ' . $service->cost[0]->value . ' * ' . $unit->id_unit }}"
                                                            >
                                                            <label for="customRadio{{$unit->id_unit}}{{ $noForService }}" class="custom-control-label">{{ $service->description }}</label><br>
                                                            <span class="text-muted text-right">
                                                                {{ $service->cost[0]->etd }} hari - <span class="pull-right">@currency($service->cost[0]->value)</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                @php $noForService = $noForService + 1; @endphp
                                                @endforeach
                                            </div>
                                        </div>

                                    @php $no = $no + 1; @endphp
                                    @endforeach
                                </div>

                                <div class="row my-3">
                                    <div class="col-md-6 col-6">
                                        <span class="text-muted font-weight-light">Berat </span>
                                    </div>
                                    <div class="col-md-6 col-6 text-right">
                                        <span class="font-weight-medium">
                                            @php
                                                echo $subtotalWeight / 1000 . " kg";
                                            @endphp
                                        </span>
                                    </div>
                                    <div class="col-md-6 col-6">
                                        <span class="text-muted font-weight-light">Total Item</span>
                                    </div>
                                    <div class="col-md-6 col-6 text-right">
                                    <span class="font-weight-medium">{{ $subtotalItem }} pcs</span>
                                    </div>
                                    <div class="col-md-6 col-6">
                                        <span class="text-muted font-weight-light">Subtotal untuk Produk</span>
                                    </div>
                                    <div class="col-md-6 col-6 text-right">
                                        <span class="font-weight-medium">@currency($subtotalProduct)</span>
                                    </div>
                                    <div class="col-md-6 col-6">
                                        <span class="text-muted font-weight-light">Subtotal Pengiriman</span>
                                    </div>
                                    <div class="col-md-6 col-6 text-right">
                                        <span class="font-weight-medium cost-subtotal-{{$unit->id_unit}}">Silahkan pilih pengiriman</span>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @php
                    $totalWeight += $subtotalWeight;
                    $totalItem += $subtotalItem;
                    $totalProduct += $subtotalProduct;
                    $totalShippingCost += $subtotalShippingCost;
                @endphp

                <input type="hidden" name="subtotal_product_{{$unit->id_unit}}" value="{{ $subtotalProduct }}">
                <input type="hidden" name="subtotal_weight_{{$unit->id_unit}}" value="{{ $subtotalWeight }}">
                <input type="hidden" name="subtotal_item_{{$unit->id_unit}}" value="{{ $subtotalItem }}">
            @endforeach

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Ringkasan</h5>
                        </div>
                        <div class="card-body">
                            <div class="row mb-3">
                                <div class="col-md-6 col-6">
                                    <span class="text-muted font-weight-light">Total Berat</span>
                                </div>
                                <div class="col-md-6 col-6 text-right">
                                    <span class="font-weight-medium">{{ $totalWeight / 1000 . ' kg' }}</span>
                                </div>
                                <div class="col-md-6 col-6">
                                    <span class="text-muted font-weight-light">Total Item</span>
                                </div>
                                <div class="col-md-6 col-6 text-right">
                                <span class="font-weight-medium">{{ $totalItem }} pcs</span>
                                </div>
                                <div class="col-md-6 col-6">
                                    <span class="text-muted font-weight-light">Total untuk Produk</span>
                                </div>
                                <div class="col-md-6 col-6 text-right">
                                    <span class="font-weight-medium">@currency($totalProduct)</span>
                                </div>
                                <div class="col-md-6 col-6">
                                    <span class="text-muted font-weight-light">Total Pengiriman</span>
                                </div>
                                <div class="col-md-6 col-6 text-right">
                                    <span class="font-weight-medium cost-total">Silahkan pilih pengiriman</span>
                                </div>

                                <div class="col-md-6 col-6">
                                    <span class="font-weight-bold">Total Tagihan</span>
                                </div>
                                <div class="col-md-6 col-6 text-right">
                                    <span class="font-weight-bold total-transaction">@currency($totalProduct)</span>
                                </div>

                            </div>

                            <input type="hidden" name="total_product" value="{{ $totalProduct }}">
                            <input type="hidden" name="total_transaction" value="{{ $totalProduct }}">
                            <input type="hidden" name="total_weight" value="{{ $totalWeight }}">
                            <input type="hidden" name="total_item" value="{{ $totalItem }}">

                            <div class="row my-3">
                                <button type="submit" class="btn btn-orange btn-sn btn-block">
                                    BUAT PESANAN
                                </button>
                                <a href="{{ url('/customer/produk') }}" class="btn btn-outline-orange btn-sn btn-block">
                                    BELANJA LAGI
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

{{-- hidden data --}}
<input type="hidden" name="default_id_kecamatan" value="{{ $myProfile->id_kecamatan }}">
<input type="hidden" name="total_unit" value="{{ $totalUnit }}">
<input type="hidden" name="total_ongkir_checked" value="0">

{{-- hidden data --}}

@include('customer.checkout.modalRequestJtr')
@include('customer.checkout.js_v2')
@endsection
