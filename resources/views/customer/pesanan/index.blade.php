@extends('customer')

@section('content')

@include('customer.pesanan.css')

<section class="content-header" style="margin-top: 70px;">
    <h4>
        <a href="{{ url('customer/dashboard') }}" style="color: black">
            <i class="fas fa-long-arrow-alt-left"></i>
        </a>
        &nbsp;&nbsp;
        Pesanan Saya
    </h4>
</section>

<section class="content">
    <div class="container-fluid">
        {{-- <div class="row wrap-category">
            <div class="col-md-12">
                <div class="scrollmenu">
                    @foreach ($status as $item)
                        <a href="{{ url('/customer/pesanan', $item->kode) }}" class="{{ $item->kode == $sttsUmum ? 'active' : '' }}">{{ $item->nama }}</a>
                    @endforeach
                </div>
            </div>
        </div> --}}

        @if (count($listOrder) == 0)
            <div class="mt-5 text-center">
                <h4 class="text-muted">
                    Tidak ada pesanan.
                </h4>
                <h4 class="text-dark">
                    <a href="{{ url('/customer/produk') }}" class="text-pink">
                        Mulai Belanja Yuk!
                    </a>
                </h4>
            </div>
        @else
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body" style="padding-top: 0px; padding-bottom: 0px;">
                            @foreach ($listOrder as $order)
                                <div class="row list-pesanan border-bottom py-2">
                                    <div class="col-md-6 col-6">
                                        <a href="{{ url('/customer/pesanan/detail', $order->no_invoice) }}" style="color: #F87119">
                                            <h4 class="dt-list-pesanan">{{ $order->no_invoice }}</h4>
                                        </a>
                                        <span class="font-weight-light text-muted">{{ HelperDataReferensi::konversiTgl($order->tgl_transaksi, 'T') }}</span>
                                    </div>
                                    <div class="col-md-6 col-6 text-right">
                                        <h4 class="font-weight-normal text-dark dt-list-pesanan">@currency($order->total_transaksi)</h4>
                                    <span class="font-weight-light text-muted">{{ ($order->status > 0) ? 'Sudah Bayar' : 'Belum Bayar' }}</span>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        {{ $listOrder->links() }}
                    </div>
                </div>
            </div>
        @endif
    </div>
</section>
@endsection
