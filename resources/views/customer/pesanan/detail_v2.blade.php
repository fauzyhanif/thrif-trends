@extends('customer')

@section('content')
<section class="content-header" style="margin-top: 70px;">
    <h4>
        <a href="{{ url('customer/pesanan') }}" style="color: grey">
            <i class="fas fa-long-arrow-alt-left"></i>
        </a>
        &nbsp;&nbsp;
        Detail Pesanan
    </h4>
</section>

<section class="content">
    <div class="container-fluid">
        @if ($transaksiByr->status == 0)
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title text-muted">
                                <i class="fas fa-receipt"></i>&nbsp; Informasi Pesanan
                            </h3>
                        </div>
                        <div class="card-body">
                            <div>
                                <p class="text-muted font-weight-light text-center" style="margin-bottom: 0px">No Invoice Anda</p>
                                <h4 class="font-weight-bold text-center">{{ $transaksiByr->no_invoice }}</h4>
                            </div>

                            <div class="mt-2">
                                <p class="text-muted pb-0 font-weight-light text-center" style="margin-bottom: 0px">Tagihan anda sebesar</p>
                                <h4 class="font-weight-bold pb-2 text-center">@currency($transaksiByr->total_transaksi)</h4>
                            </div>
                            
                            <a href="{{ url('/customer/pembayaran', $transaksiByr->no_invoice) }}" class="btn btn-orange btn-sm btn-block">
                                BAYAR SEKARANG
                            </a>                             
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @foreach ($transaksiByr->_transaksis as $transaksi)        
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">{{ $transaksi->_unit->nama }}</h5> <br>
                            <span class="text-muted">{{ $transaksi->_unit->_city->city_name }}</span>
                        </div>
                        <div class="card-body">
                            @foreach ($transaksi->_details as $detail)
                                <div class="border-bottom">
                                    <div class="row my-2">
                                        <div class="col-md-7">
                                            <span class="font-weight-normal text-dark font-13">{{ $detail->_produk->nama }}</span>
                                        </div>
                                        <div class="col-md-3 col-6">
                                            <span class="font-weight-light text-muted">{{ $detail->qty }} x @currency($detail->harga)</span>
                                        </div>
                                        <div class="col-md-2 col-6 text-right">
                                            <span class="font-weight-normal text-dark">@currency($detail->jumlah)</span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            <div class="row my-3">
                                <div class="col-md-6 col-6">
                                    <span class="text-muted font-weight-light">Berat </span>
                                </div>
                                <div class="col-md-6 col-6 text-right">
                                    <span class="font-weight-medium">
                                        @php
                                            echo $transaksi->ttl_berat / 1000 . " kg";
                                        @endphp
                                    </span>
                                </div>
                                <div class="col-md-6 col-6">
                                    <span class="text-muted font-weight-light">Total Item</span>
                                </div>
                                <div class="col-md-6 col-6 text-right">
                                    <span class="font-weight-medium">{{ $transaksi->ttl_qty }} pcs</span>
                                </div>

                                <div class="col-md-6 col-6">
                                    <span class="text-muted font-weight-light">Subtotal untuk Produk</span>
                                </div>
                                <div class="col-md-6 col-6 text-right">
                                    <span class="font-weight-medium">@currency($transaksi->ttl_belanja)</span>
                                </div>
                                
                                <div class="col-md-6 col-6">
                                    <span class="text-muted font-weight-light">Subtotal Pengiriman</span>
                                </div>
                                <div class="col-md-6 col-6 text-right">
                                    <span class="font-weight-medium cost-subtotal">@currency($transaksi->ttl_ongkir)</span>
                                </div>
                                
                                <div class="col-md-6 col-6">
                                    <span class="font-weight-bold">Total Transaksi</span>
                                </div>
                                <div class="col-md-6 col-6 text-right">
                                    <span class="font-weight-bold cost-subtotal">@currency($transaksi->ttl_transaksi)</span>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        <div class="row">
            <div class="col-lg-12">
                @if ($transaksiByr->status == 0)
                    <a href="{{ url('/customer/pembayaran', $transaksiByr->no_invoice) }}" class="btn btn-orange btn-sm btn-block my-3">
                        BAYAR SEKARANG
                    </a>
                @endif
            </div>
        </div>
    </div>
</section>

@include('customer.pesanan.modalKonfirmasiPesananSampai')
<script>
function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
}
</script>
@endsection
