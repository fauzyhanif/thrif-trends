<div class="modal fade" id="konfirmasi-pesanan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ url('/dist/pesanan/konfirmasi-sampai') }}" method="POST">

                {{-- hidden data --}}
                @csrf
                <input type="hidden" name="no_invoice" value="{{ $transaksiByr->no_invoice }}">
                <input type="hidden" name="stts_umum" value="3">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Konfirmasi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Apakah pesanan anda telah sampai?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Ya, Konfirmasi</button>
                </div>
            </form>
        </div>
    </div>
</div>
