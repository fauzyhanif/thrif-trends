<style>
    .dt-list-pesanan {
        font-size: 16px !important;
    }

    div.scrollmenu {
        color: grey;
        background-color: #fff;
        overflow: auto;
        white-space: nowrap;
    }

    div.scrollmenu a {
        display: inline-block;
        color: grey;
        text-align: center;
        padding: 14px;
        text-decoration: none;
    }

    div.scrollmenu a.active {
        color: #fff;
        background-color: #777;
    }

    div.scrollmenu a:hover {
        color: #fff;
        background-color: #777;
    }

    @media (max-width: 768px) {
        div.scrollmenu {
            color: grey;
            background-color: #fff;
            overflow: auto;
            white-space: nowrap;
        }
    }

</style>
