@extends('customer')

@section('content')
<section class="content-header" style="margin-top: 70px;">
    <h4>
        Keranjang Anda
    </h4>
</section>

@csrf

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if ($countMyCart != 0)
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Daftar Keranjang</h3>
                        <span class="text-muted float-right">{{ $countMyCart }} Item</span>
                    </div>

                    <div class="card-body" style="padding-top: 0px; padding-bottom: 0px">
                        @php
                            $shoppingAmount = 0;
                            $itemAmount = 0;
                        @endphp
                        @foreach ($myCart as $item)
                            <div class="border-bottom wrap-list-cart">

                                {{-- hidden data --}}
                                <input type="hidden" name="id" value="{{ $item->id }}">
                                <input type="hidden" name="kode_produk" value="{{ $item->kode_produk }}">
                                <input type="hidden" name="harga" value="{{ $item->harga }}">
                                <input type="hidden" name="jumlah" value="{{ $item->jumlah }}">


                                <div class="row my-3">
                                    <div class="col-md-4">
                                        <h5 class="font-weight-normal text-dark">{{ $item->nama }}</h5>
                                    </div>
                                    <div class="col-md-4 col-6">
                                        <span class="font-weight-light text-muted">@currency($item->harga)</span>
                                    </div>
                                    <div class="col-md-2 col-4">
                                        <input type="number" name="qty" class="form-control" value="{{ $item->qty }}" style="height: 75%">
                                    </div>
                                    <div class="col-md-2 col-2 text-center">
                                        <button class="btn btn-danger btn-xs remove-cart">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        @php
                            $shoppingAmount = $shoppingAmount + $item->jumlah;
                            $itemAmount = $itemAmount + $item->qty;
                        @endphp
                        @endforeach


                        <div class="row my-3">
                            <div class="col-md-6 col-6">
                                <span class="text-muted font-weight-light">Total Item</span>
                            </div>
                            <div class="col-md-6 col-6 text-right">
                                <span class="font-weight-normal item-amount">{{ $itemAmount }} pcs</span>
                            </div>
                            <div class="col-md-6 col-6">
                                <span class="text-muted font-weight-light">Sub Total</span>
                            </div>
                            <div class="col-md-6 col-6 text-right">
                                <span class="font-weight-normal shopping-amount">@currency($shoppingAmount)</span>
                            </div>
                        </div>

                        <div class="row my-3">
                            <a href="{{ url('/customer/checkout') }}" class="btn btn-orange btn-sn btn-block">
                                CHECKOUT
                            </a>
                            <a href="{{ url('/customer/produk') }}" class="btn btn-outline-orange btn-sn btn-block">
                                BELANJA LAGI
                            </a>
                        </div>
                    </div>
                </div>
                @else
                <div class="text-center mt-5">
                    <h4 class="text-muted">Keranjangmu masih kosong</h4>
                    <h4 class="text-dark">
                        <a href="{{ url('/customer/produk') }}" class="text-pink">
                            Mulai Belanja Yuk!
                        </a>
                    </h4>
                </div>
                @endif
            </div>
        </div>
    </div>
</section>

@include('customer.keranjang.js')
@endsection
