<script>
    $(function () {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-full-width",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "2000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    })

    function addToCart( idProduk ) {  
        $.ajax({
            type: 'GET',
            url: {!! json_encode(url('/check-session-with-js')) !!},
            dataType: "JSON",
            success: function(res){

                if (res != null) {
                    var url = {!! json_encode(url('/customer/add-to-cart')) !!};
                    var data = {
                        "_token" : $("input[name='_token']").val(),
                        "kode_produk" : idProduk
                    };

                    $.ajax({
                        type: 'POST',
                        url: url,
                        dataType: "JSON",
                        data: data,
                        success: function(res){
                            var type = res.status;
                            var text = res.text;
                            if (type == 'success') {
                                toastr.success(text);

                                // reload header
                                reloadHeaderCountCart();
                                reloadHeaderListCart();
                                appendUrlAndTextHeaderCart();
                            } else {
                                toastr.danger(text);
                            }
                        }
                    });
                } else {
                    // redirect to login ulang page
                    var base = {!! json_encode(url('/login-ulang')) !!};
                    document.location.href = base;
                }
            }
        })

        
    }

    function appendUrlAndTextHeaderCart() {
        var url = {!! json_encode(url('/customer/keranjang')) !!};
        $('.bottom-of-header-cart').attr("href", url);
        $('.bottom-of-header-cart').text("Lihat Semua");
    }

    function reloadHeaderCountCart() {
        var url = {!! json_encode(url('/customer/reload-header-count-cart')) !!};
        $.ajax({
            type: 'GET',
            url: url,
            dataType: "JSON",
            success: function(res){
                $('.header-count-cart').text(res)
            }
        });
    }

    function reloadHeaderListCart() {
        var url = {!! json_encode(url('/customer/reload-header-list-cart')) !!};
        var urlImage = {!! json_encode(url('/foto-produk')) !!};
        $.ajax({
            type: 'GET',
            url: url,
            dataType: "JSON",
            success: function(res){
                var i;
                var element = '';
                for (var x in res) {
                    var y = urlImage + '/' + res[x].thumbnail;
                    element += '<div class="media my-2">';
                    element += '<img src="'+ y +'" alt="User Avatar" class="mr-3 img-circle" style="width: 37px;">';
                    element += '<div class="media-body">';
                    element += '<h3 class="dropdown-item-title">' + res[x].nama + '</h3>';
                    element += '<p class="text-sm text-muted"> Rp. ' + formatRupiah(res[x].harga) + '</p>';
                    element += '</div>';
                    element += '</div>';
                }
                $('.header-list-cart').html(element)
            }
        });
    }

    function formatRupiah(bilangan){
        var	number_string = bilangan.toString(),
        sisa 	= number_string.length % 3,
        rupiah 	= number_string.substr(0, sisa),
        ribuan 	= number_string.substr(sisa).match(/\d{3}/g);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        return rupiah;
    }
</script>
