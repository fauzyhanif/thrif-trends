@extends('customer')

@section('content')
@include('customer.pesanan.css')

<style>
    .card-img-top {
        height : 200px;
    }
</style>

<section class="content-header" style="margin-top: 70px;">
    <h1>
        Produk
    </h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form method="POST" action="{{ url('customer/produk-search') }}">
                    @csrf
                    <div class="input-group input-group">
                        <input class="form-control" type="text" name="key" placeholder="Cari Produk" aria-label="Search">
                        <div class="input-group-append">
                            <button class="btn btn-orange" type="submit">
                                <i class="fas fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row mt-4 wrap-category">
            <div class="col-md-12">
                <h5>Kategori Produk</h5>
            </div>

            <div class="col-md-12">
                <div class="scrollmenu">
                    <a href="{{ url('/customer/produk') }}" class="{{ $kategori == '*' ? 'active' : '' }}">Semua Kategori</a>
                    @foreach ($categories as $category)
                        <a href="{{ url('/customer/produk', $category->id) }}" class="{{ $kategori == $category->id ? 'active' : '' }}">{{ $category->nama }}</a>
                    @endforeach
                </div>
            </div>
        </div>

        @if (count($products) == 0)
            <div class="text-center mt-5">
                <h4 class="text-muted">Tidak ditemukan produk di kategori ini.</h4>
                <h4 class="text-dark">
                    <a href="{{ url('/customer/produk') }}" class="text-orange">
                        Kepoin produk lain Yuk!
                    </a>
                </h4>
            </div>
        @else
            <div class="row mt-4">
                @foreach ($products as $product)
                    <div class="col-md-3 col-6">
                        <div class="card">
                            <img class="card-img-top" src="{{ url('foto-produk', $product->thumbnail) }}" alt="Card image cap">
                            <div class="card-body" style="padding: 10px !important">
                                <p class="card-title text-dark">{{ Str::limit($product->nama, 13) }}</p>
                                <p class="card-text text-muted">@currency($product->harga_jual)</p>

                                <p class="card-text text-muted">{{ $product->_unit->_city->city_name }}</p>

                                <div class="row">
                                    <div class="col-md-6 col-6">
                                        <a href="{{ url('customer/produk/detail', $product->id) }}" type="button" class="btn btn-light btn-sm btn-block">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-6 col-6">
                                        @if ($product->stok <= 0)
                                            <p class="text-muted text-center">Habis!</p>
                                        @else
                                            <button type="button" class="btn btn-orange btn-sm btn-block" onclick="addToCart('{{ $product->id }}')">
                                                <i class="fas fa-cart-plus"></i>
                                            </button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row mt-4 justify-content-center">
                <div class="col-md-3 col-12">
                    {{ $products->links() }}
                </div>
            </div>
        @endif
    </div>
</section>

@include('customer.produk.js')
@endsection
