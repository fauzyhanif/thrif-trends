@extends('distributor')

@section('content')
<section class="content-header" style="margin-top: 70px;">

</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="row my-2 text-center">
                    <div class="col-md-12">
                        <h2 class="font-weight-bold">
                            Mohon maaf anda harus login ulang karna sesi sudah habis.
                        </h2>
                    </div>
                </div>

                <div class="row mt-2 mb-1 text-center">
                    <div class="col-md-12">
                        <a href="{{ url('/auth') }}" class="text-pink" style="text-decoration: underline">Login Ulang</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
