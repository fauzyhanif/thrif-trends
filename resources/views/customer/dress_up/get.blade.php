<div class="row text-center">
    <div class="col-12 mb-2">
        <h5 class="my-2">Box Dress Up</h5>
    </div>
    <div class="col-5">
        <img src="{{ url('public/img/patung-1.png') }}" alt="">
    </div>
    <div class="col-7">
        <div class="row">
            <div class="col-12 mb-0">
                @if ($upperClothes)
                    <img src="{{ url('foto-produk', $upperClothes->_produk->thumbnail) }}" alt="" class="img-fluid" style="width: 50%">
                @else
                    <span class="text-center m-auto">Belum ada produk atasan</span>
                @endif
            </div>
            <div class="col-12">
                @if ($bottomClothes)
                    <img src="{{ url('foto-produk', $bottomClothes->_produk->thumbnail) }}" alt="" class="img-fluid" style="width: 50%">
                @else
                    <span class="text-center my-4">Belum ada produk bawahan</span>
                @endif
            </div>
        </div>
    </div>
</div>


<div class="row my-3">
    <div class="col-12">
        <div class="alert alert-default-success">
            <h5 class="text-center">Matching Score</h5>
            <h3 class="font-weight-bold text-center">{{ $matchingScore }}</h3>
            <p class="text-center">
                @if ($matchingScore <= 50)
                    Sepertinya kombinasi ini kurang cocok. Coba padukan dengan pilihan yang lebih pas untuk menciptakan gaya yang sempurna!
                @elseif ($matchingScore > 50 && $matchingScore <= 70)
                    sepertinya atasan dan bawahan ini sudah cukup baik. Namun, jangan ragu untuk mencoba variasi lain untuk menemukan gaya yang paling Anda sukai!
                @elseif ($matchingScore > 70)
                    Kombinasi ini sungguh luar biasa! Atasan dan bawahan ini saling melengkapi dengan sempurna, menciptakan tampilan yang benar-benar memukau!
                @endif
            </p>
        </div>
    </div>
</div>

<div class="row mt-4">
    <div class="col-12 mb-2">
        <h5>Produk Dress Up</h5>
    </div>
    <div class="col-6">
        <div class="card">
            @if ($upperClothes)
                <img src="{{ url('foto-produk', $upperClothes->_produk->thumbnail) }}" alt="" class="card-img-top img-fluid" style="width: 100%">
                <div class="card-body" style="padding: 10px !important">
                    <p class="card-title text-dark">{{ $upperClothes->_produk->nama }}</p>
                    <p class="card-text text-muted">@currency($upperClothes->_produk->harga_jual)</p>

                    <div class="row">
                        <div class="col-md-6 col-6">
                            <a href="{{ url('customer/produk/detail/', $upperClothes->id_produk) }}" type="button" class="btn btn-light btn-sm btn-block" >
                                <i class="fas fa-eye"></i>
                            </a>
                        </div>
                        <div class="col-md-6 col-6">
                            <button type="button" class="btn btn-orange btn-sm btn-block" onclick="addToCart('{{$upperClothes->id_produk}}')">
                                <i class="fas fa-cart-plus"></i>
                            </button>
                        </div>
                    </div>
                </div> 
            @else
                <span class="text-center m-auto">Belum ada produk atasan</span>
            @endif
            
        </div>
    </div>

    <div class="col-6">
        <div class="card">
            @if ($bottomClothes)
                <img src="{{ url('foto-produk', $bottomClothes->_produk->thumbnail) }}" alt="" class="card-img-top img-fluid" style="width: 100%">
                <div class="card-body" style="padding: 10px !important">
                    <p class="card-title text-dark">{{ $bottomClothes->_produk->nama }}</p>
                    <p class="card-text text-muted">@currency($bottomClothes->_produk->harga_jual)</p>

                    <div class="row">
                        <div class="col-md-6 col-6">
                            <a href="{{ url('customer/produk/detail/', $bottomClothes->id_produk) }}" type="button" class="btn btn-light btn-sm btn-block" >
                                <i class="fas fa-eye"></i>
                            </a>
                        </div>
                        <div class="col-md-6 col-6">
                            <button type="button" class="btn btn-orange btn-sm btn-block" onclick="addToCart('{{$bottomClothes->id_produk}}')">
                                <i class="fas fa-cart-plus"></i>
                            </button>
                        </div>
                    </div>
                </div> 
            @else
                <span class="text-center m-auto">Belum ada produk bawahan</span>
            @endif
        </div>
    </div>
</div>