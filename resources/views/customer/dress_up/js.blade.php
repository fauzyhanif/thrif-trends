<script>
$( document ).ready(function() {
    getDressUp();
});

function addToDressUp( idProduk ) {  
    $.ajax({
        type: 'GET',
        url: {!! json_encode(url('/check-session-with-js')) !!},
        dataType: "JSON",
        success: function(res){

            if (res != null) {
                var url = {!! json_encode(url('/customer/dress-up/create')) !!};
                var data = {
                    "_token" : $("input[name='_token']").val(),
                    "id_produk" : idProduk
                };

                $.ajax({
                    type: 'POST',
                    url: url,
                    dataType: "JSON",
                    data: data,
                    success: function(res){
                        getDressUp();
                        var type = res.status;
                        var text = res.text;
                        if (type == 'success') {
                            toastr.success(text);
                        } else {
                            toastr.danger(text);
                        }
                    }
                });
            } else {
                // redirect to login ulang page
                var base = {!! json_encode(url('/login-ulang')) !!};
                document.location.href = base;
            }
        }
    })        
}


function getDressUp() {  
    $.ajax({
        type: 'GET',
        url: {!! json_encode(url('/customer/dress-up/get')) !!},
        dataType: "html",
        success: function(res){
            $('#view-dress-up').html(res);
            scrollToTop();
        }
    })        
}

function scrollToTop() {
    window.scrollTo({
        top: 0,
        behavior: 'smooth' // Use 'smooth' for smooth scrolling
    });
}


</script>