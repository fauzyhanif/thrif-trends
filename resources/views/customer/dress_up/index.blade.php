@extends('customer')
@section('content')


<style>
    .custom-card {
      width: 100%;
      height: 100%;
      object-fit: cover;
    }

    .card-img-top {
        height : 200px;
    }

    #cardContainer {
        overflow-x: auto;
        white-space: nowrap;
    }

    .col-produk {
        display: inline-block;
    }

    #col {
        flex: 0 0 auto;
    }

    .card-container {
        display: flex;
        overflow-x: auto;
        padding: 10px;
        white-space: nowrap;
    }
</style>

@csrf

<section class="content-header text-dark" style="margin-top: 70px;">
    <h4>
        Dress Up
    </h4>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row pb-4">
            <div class="col-lg-5">
                <div class="card h-100">
                    <div class="card-body" id="view-dress-up">
                        
                    </div>
                </div>
            </div>

            <div class="col-lg-7">
                <div class="card">
                    <div class="card-body">
                        <div class="alert alert-default-info">
                            Silahkan klik pakaian dibawah untuk display produk di box dress up
                        </div>

                        <div class="row flex-nowrap card-container mb-4">
                            @foreach ($upperClothes as $upperClothe)
                                <div class="col-lg-3 col-6" id="col">
                                    <div class="card">
                                        <img src="{{ url('foto-produk', $upperClothe->thumbnail) }}" alt="Card image cap" class="card-img-top img-fluid" style="width: 100%">
                                        <div class="card-body" style="padding: 10px !important">
                                            <p class="card-text text-muted">@currency($upperClothe->harga_jual)</p>
            
            
                                            <button type="button" class="btn btn-orange btn-sm btn-block" onclick="addToDressUp('{{ $upperClothe->id }}')">
                                                <i class="fas fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <div class="row flex-nowrap card-container">
                            @foreach ($bottomClothes as $bottomClothe)
                                <div class="col-lg-3 col-6" id="col">
                                    <div class="card">
                                        <img src="{{ url('foto-produk', $bottomClothe->thumbnail) }}" alt="Card image cap" class="card-img-top img-fluid" style="width: 100%">
                                        <div class="card-body" style="padding: 10px !important">
                                            <p class="card-text text-muted">@currency($upperClothe->harga_jual)</p>
            
            
                                            <button type="button" class="btn btn-orange btn-sm btn-block" onclick="addToDressUp('{{ $bottomClothe->id }}')">
                                                <i class="fas fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('customer.dress_up.js')
@include('customer.produk.js')
@endsection
