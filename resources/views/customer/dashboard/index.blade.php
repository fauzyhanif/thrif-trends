@extends('customer')

@section('content')

@include('customer.dashboard.css')
<section class="content-header text-dark" style="margin-top: 70px;">
    <h4>
        Dashboard
    </h4>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card  card-outline">
                    <div class="card-body box-profile">
                        <div class="row">
                            <div class="col-md-3 col-3 text-center">
                                <img class="profile-user-img img-fluid img-circle" src="{{ url('public/img/user.png') }}" style="width: 140px;" alt="User profile picture">
                            </div>

                            <div class="col-md-9 col-9 mt-4 dash-profile-name">
                            <h1 class="font-weight-medium text-dark dash-profile-name-text">{{ $myProfile->nama }}</h1>
                            <h4 class="text-muted font-weight-light dash-profile-name-origin">
                                <i class="fas fa-map-marker-alt"></i> &nbsp;
                                {{ $myProfile->city_name }}
                            </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 mb-3">
                <a href="{{ url('customer/profile') }}" class="btn btn-default btn-block">
                    LIHAT PROFIL
                </a>
            </div>

            <div class="col-md-12 mb-3">
                <a href="{{ url('customer/produk') }}" class="btn btn-outline-orange btn-block">
                    MULAI BELANJA
                </a>
            </div>

            <div class="col-md-4 col-12">
                <div class="info-box mb-3 bg-default">
                    <span class="info-box-icon text-secondary"><i class="fas fa-wallet"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text text-muted font-weight-light">Total Transaksi</span>
                        <span class="info-box-number text-dark font-weight-normal">@currency($total->ttl_omset)</span>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-6">
                <div class="info-box mb-3 bg-default">
                    <div class="info-box-content">
                        <span class="info-box-text text-muted font-weight-light">Jumlah Belanja</span>
                        <span class="info-box-number text-dark font-weight-normal">{{ $total->ttl_transaksi . 'x' }}</span>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-6">
                <div class="info-box mb-3 bg-default">
                    <div class="info-box-content">
                        <span class="info-box-text text-muted font-weight-light">Transaksi Bulan Ini</span>
                        <span class="info-box-number text-dark font-weight-normal">@currency($omsetThisMonth)</span>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <h5 class="float-left text-dark">Pesanan saya</h5>
                <span class="float-right text-muted">
                    <a href="{{ url('/customer/pesanan') }}" style="color: grey">
                        Lihat semua
                    </a>
                </span>
            </div>

            <div class="col-md-12 mt-2">
                <div class="card">
                    <div class="card-body" style="padding-top: 0px; padding-bottom: 0px;">
                        @foreach ($listOrder as $order)
                            <div class="row list-pesanan border-bottom py-2">
                                <div class="col-md-6 col-6">
                                    <a href="{{ url('/customer/pesanan/detail', $order->no_invoice) }}" style="color: #F87119">
                                        <h4 class="dt-list-pesanan">{{ $order->no_invoice }}</h4>
                                    </a>
                                    <span class="font-weight-light text-muted">{{ HelperDataReferensi::konversiTgl($order->tgl_transaksi, 'T') }}</span>
                                   
                                </div>
                                <div class="col-md-6 col-6 text-right">
                                    <h4 class="font-weight-normal text-dark dt-list-pesanan">@currency($order->total_transaksi)</h4>
                                <span class="font-weight-light text-muted">{{ ($order->status > 0) ? 'Sudah Bayar' : 'Belum Bayar' }}</span>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
