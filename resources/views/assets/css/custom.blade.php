<style>
.btn-pink {
  color: #ffffff;
  background-color: #FF8CB6;
  border-color: #FF8CB6;
  box-shadow: none;
}

.btn-pink:hover {
  color: #ffffff;
  background-color: #F64F8C;
  border-color: #F64F8C;
}

.btn-outline-pink {
  color: #FF8CB6;
  border-color: #FF8CB6;
}

.btn-outline-pink:hover {
  color: #ffffff;
  background-color: #F64F8C;
  border-color: #F64F8C;
}

.cst-brand-text {
  color: #fff;
}

.cst-nav-link {
  color: #fff !important;
}

.navbar-pink {
  background-color: #FF8CB6 !important;
}

/*  */
.btn-green {
  color: #F3B790;
  background-color: #7BA697;
  border-color: #7BA697;
  box-shadow: none;
}

.btn-green:hover {
  color: #F3B790;
  background-color: #7BA697;
  border-color: #7BA697;
}

.btn-outline-green {
  color: #7BA697;
  border-color: #7BA697;
}

.btn-outline-green:hover {
  color: #F3B790;
  background-color: #7BA697;
  border-color: #7BA697;
}

.btn-orange {
  color: #fff;
  background-color: #F3B790;
  border-color: #F3B790;
  box-shadow: none;
}

.btn-orange:hover {
  color: #fff;
  background-color: #DB5F0F;
  border-color: #DB5F0F;
}

.btn-outline-orange {
  color: #4A6F62;
  border-color: #F3B790;
}

.btn-outline-orange:hover {
  color: #fff;
  background-color: #F3B790;
  border-color: #F3B790;
}

.cst-brand-text {
  color: #F3B790;
}

.cst-nav-green {
  color: #F3B790 !important;
}

.navbar-green {
  background-color: #7BA697 !important;
  color: #F3B790 !important;
}

.content-header {
  margin-top: 67px !important;
}

</style>
