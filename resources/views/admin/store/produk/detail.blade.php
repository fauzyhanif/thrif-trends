@extends('admin')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="{{ url('/admin/produk') }}" class="btn btn-default bt-sm">
                    <i class="fas fa-long-arrow-alt-left"></i> Kembali
                </a>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Referensi</a></li>
                    <li class="breadcrumb-item active">Produk</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
            <!-- Default box -->
                <div class="card card-solid">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <h3 class="d-inline-block d-sm-none">LOWA Men’s Renegade GTX Mid Hiking Boots Review</h3>
                                <div class="col-12">
                                    <img src="{{ url('foto-produk', $data->thumbnail) }}" style="width: 100%; height: auto;" class="product-image" alt="Product Image">
                                </div>
                                <div class="col-12 product-image-thumbs">
                                    <div class="product-image-thumb active"><img src="{{ url('foto-produk', $data->thumbnail) }}" style="width: 100%; height: auto;" alt="Product Image"></div>
                                    @foreach ($dtFoto as $item)
                                        <div class="product-image-thumb"><img src="{{ url('foto-produk', $item->nama_file) }}" style="width: 100%; height: auto;" alt="Product Image"></div>
                                    @endforeach
                                </div>
                            </div>

                            <div class="col-12 col-sm-6">
                                <h3 class="my-3">{{ $data->nama }}</h3>
                                <p>{{ $data->deskripsi }}</p>

                                <hr>
                                <div class="bg-gray py-2 px-3 mt-4">
                                    <h2 class="mb-0">
                                        Rp. <?= number_format($data->harga_jual,2,',','.'); ?>
                                    </h2>
                                </div>

                                <div class="mt-4">
                                    <a href="{{ url('/admin/produk/form-edit', $data->id) }}" class="btn btn-primary btn-sm btn-flat">
                                        <i class="fas fa-edit fa-lg mr-2"></i>
                                        Edit Produk
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
