@extends('admin')

@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 ml-0 text-dark">Produk</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Referensi</a></li>
          <li class="breadcrumb-item active">Produk</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <h5><i class="icon fas fa-check"></i> Berhasil</h5>
                    <p>{{ $message }}</p>
                </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header with-border">
                        <h3 class="card-title">
                            List Data
                        </h3>
                        <div class="card-tools">
                            <a href="{{ url('/admin/produk/form-add') }}" class="btn btn-success btn-sm pull-right">
                                <i class="fa fa-plus"></i> Tambah Produk
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped" id="produk-table">
                            <thead>
                                <th width="15%">Kode Produk</th>
                                <th>Nama Produk</th>
                                <th>Kategori</th>
                                <th>Stok</th>
                                <th>Harga Jual</th>
                                <th>Status</th>
                                <th width="8%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
</section>

<script>
    $(function() {
        $('#produk-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: 'data-produk-json',
            columns: [
                { data: 'kode', name: 'a.kode', searchable: true },
                { data: 'nama', name: 'a.nama', searchable: true },
                { data: 'nm_kategori', name: 'b.nm_kategori', searchable: false },
                { data: 'stok', name: 'a.stok', searchable: false },
                { data: 'harga_jual', name: 'a.harga_jual', searchable: false },
                { data: 'status', name: 'status', searchable: false },
                { data: 'link', name: 'link', searchable: false },
            ]
        });

    });
    </script>
@endsection
