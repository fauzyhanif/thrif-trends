@extends('admin')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-12">
                <a href="{{ url('/admin/laporan/laba-kategori-produk') }}" class="btn btn-default btn-sm float-left">
                    <i class="fas fa-long-arrow-alt-left"></i> Kembali
                </a>
            </div>
            <div class="col-sm-12">
                <h1 class="m-0 ml-0 text-dark">Laporan Laba Kategori Produk <b>{{ $category->nama }}</b></h1>
            </div>
            <div class="col-sm-12">
                <h5 class="text-muted">Data tanggal : {{ HelperDataReferensi::konversiTgl($firstDay, 'T') }} s/d {{ HelperDataReferensi::konversiTgl($lastDay, 'T') }}</h5>
            </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-striped" id="data-table">
                            <thead>
                                <th>Nama Produk</th>
                                <th>Stok</th>
                                <th>Terjual</th>
                                <th>Jumlah Laba</th>
                            </thead>
                            <tbody>
                                @php
                                    $ttl_stok = 0;
                                    $ttl_terjual = 0;
                                    $ttl_keuntungan = 0;
                                @endphp
                                @foreach ($datas as $data)
                                    <tr>
                                        <td>{{ $data->nama }}</td>
                                        <td>{{ $data->stok }} pcs</td>
                                        <td>{{ $data->terjual }} pcs</td>
                                        <td>@currency($data->keuntungan)</td>
                                    </tr>
                                @php
                                    $ttl_stok = $ttl_stok + $data->stok;
                                    $ttl_terjual = $ttl_terjual + $data->terjual;
                                    $ttl_keuntungan = $ttl_keuntungan + $data->keuntungan;
                                @endphp
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td><b>Total</b></td>
                                    <td>{{ $ttl_stok }} pcs</td>
                                    <td>{{ $ttl_terjual }} pcs</td>
                                    <td>@currency($ttl_keuntungan)</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
</section>
@endsection
