@extends('admin')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 ml-0 text-dark">Usergroup</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Referensi</a></li>
                    <li class="breadcrumb-item active">Usergroup</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <h5><i class="icon fas fa-check"></i> Berhasil</h5>
                    <p>{{ $message }}</p>
                </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header with-border">
                        <h3 class="card-title">
                            List Data
                        </h3>
                        <div class="card-tools">
                            <a href="{{ url('/usergroup/form-add') }}" class="btn btn-primary btn-sm pull-right">
                                <i class="fa fa-plus"></i> Tambah Usergroup
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <th>ID Usergroup</th>
                                <th>Nama Usergroup</th>
                                <th>Aksi</th>
                            </thead>
                            <tbody>
                                @foreach ($datas as $data)
                                <tr>
                                    <td>#{{ $data->id_usergroup }}</td>
                                    <td>{{ $data->nama }}</td>
                                    <td>
                                        <a href="{{ url('usergroup/form-edit', $data->id_usergroup) }}" class="btn btn-default btn-xs">
                                            <i class="fa fa-edit"></i> Edit
                                        </a>
                                        <a href="{{ url('usergroup/delete', $data->id_usergroup) }}" class="btn btn-default btn-xs">
                                            <i class="fa fa-trash"></i> Hapus
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
</section>
@endsection
