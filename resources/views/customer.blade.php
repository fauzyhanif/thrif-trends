@include('layouts.customer.header')
<div class="content-wrapper">
    <div class="container">
        @yield('content')

    </div>
</div>
@include('layouts.customer.footer')
