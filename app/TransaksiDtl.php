<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiDtl extends Model
{
    protected $table = "transaksi_dtl";
    protected $fillable = [
        "id",
        "no_invoice",
        "kode_produk",
        "qty",
        "harga",
        "jumlah"
    ];

    public function _produk()
    {
        return $this->belongsTo(Produk::class, 'kode_produk', 'id');     
    }
}
