<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class SysRefUser extends Model
{
    protected $table = 'sys_ref_user';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'id_unit',
        'nama',
        'id_distributor',
        'username',
        'email',
        'password',
        'id_usergroup',
        'email_token',
        'email_verified_at',
        'is_aktif'
    ];
}
