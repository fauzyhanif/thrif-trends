<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = "produk";
    protected $fiilable = [
        "id",
        "id_unit",
        "kode",
        "kategori_produk",
        "nama",
        "thumbnail",
        "berat",
        "stok",
        "harga_modal",
        "harga_jual",
        "is_aktif",
        "created_at",
        "updated_at",
    ];

    public function _unit()
    {
        return $this->belongsTo(SysRefSetting::class, 'id_unit', 'id_unit');    
    }

    public function _kategori_produk()
    {
        return $this->belongsTo(KategoriProduk::class, 'kategori_produk', 'id');    
    }
}
