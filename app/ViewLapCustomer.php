<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViewLapCustomer extends Model
{
    protected $table = "view_lap_customer";
    protected $fiilable = [
        "id",
        "nama",
        "city_name",
        "jml_omset",
        "jml_qty",
        "order_day",
    ];
}
