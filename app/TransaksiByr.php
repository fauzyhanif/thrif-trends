<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiByr extends Model
{
    protected $table = "transaksi_byr";
    protected $fillable = [
        "id",
        "id_customer",
        "tgl_transaksi",
        "no_invoice",
        "total_transaksi",
        "status",
    ];

    public function _customer()
    {
        return $this->belongsTo(Customer::class, 'id_customer');    
    }

    public function _transaksis()
    {
        return $this->hasMany(Transaksi::class, 'id_transaksi_byr', 'id');    
    }
}
