<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MatchingScore extends Model
{
    protected $table = "matching_score";
    protected $fillable = [
        "id_produk_atasan",
        "id_produk_bawahan",
        "score",
        "created_at",
        "updated_at",
    ];

    public function _produk_atasan()
    {
        return $this->belongsTo(Produk::class, 'id_produk_atasan');    
    }

    public function _produk_bawahan()
    {
        return $this->belongsTo(Produk::class, 'id_produk_bawahan');    
    }
}

