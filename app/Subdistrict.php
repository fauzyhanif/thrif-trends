<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subdistrict extends Model
{
    protected $table = "tb_ro_subdistricts";
    protected $fiilable = [
        "subdistrict_id",
        "city_id",
        "subdistrict_name"
    ];
}
