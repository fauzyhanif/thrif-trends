<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Session;

use Closure;

class Distributor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::has('username') && Session::get('usergroup_aktif') == '3') {
            return $next($request);
        }

        return redirect('/auth');
    }
}
