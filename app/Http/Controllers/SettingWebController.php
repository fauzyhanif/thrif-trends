<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\SysRefSetting;
use Illuminate\Support\Facades\Session;

class SettingWebController extends Controller
{
    public function index()
    {
        $data = DB::table('sys_ref_setting')
            ->leftJoin('tb_ro_provinces', 'sys_ref_setting.id_provinsi', '=',  'tb_ro_provinces.province_id')
            ->leftJoin('tb_ro_cities', 'sys_ref_setting.id_kota', '=',  'tb_ro_cities.city_id')
            ->leftJoin('tb_ro_subdistricts', 'sys_ref_setting.id_kecamatan', '=',  'tb_ro_subdistricts.subdistrict_id')
            ->select('sys_ref_setting.nama', 'sys_ref_setting.no_hp_1', 'sys_ref_setting.no_hp_2',
                    'sys_ref_setting.alamat', 'sys_ref_setting.kodepos', 'tb_ro_provinces.province_name', 'tb_ro_cities.city_name', 'tb_ro_subdistricts.subdistrict_name')
            ->where('sys_ref_setting.id_unit', Session::get('id_unit'))
            ->first();

        return view('admin.settingWeb.index', \compact('data'));
    }

    public function formEdit()
    {
        $data = SysRefSetting::find(Session::get('id_unit'));

        $dtProvinsi = DB::table('tb_ro_provinces')
            ->orderBy('province_name', 'ASC')
            ->get();

        $dtKota = DB::table('tb_ro_cities')
            ->where('province_id', '=', $data->id_provinsi)
            ->orderBy('city_name', 'ASC')
            ->get();

        $dtKecamatan = DB::table('tb_ro_subdistricts')
            ->where('city_id', '=', $data->id_kota)
            ->orderBy('subdistrict_name', 'ASC')
            ->get();

        return view('admin.settingWeb.formEdit', \compact('data', 'dtProvinsi', 'dtKota', 'dtKecamatan'));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'nama'=>'required',
            'no_hp_1'=>'required',
            'email'=>'required',
            'alamat'=>'required',
            'kodepos'=>'required',
            'id_kecamatan'=>'required',
            'id_kota'=>'required',
            'id_provinsi'=>'required'
        ]);

        $oldData = SysRefSetting::find($id);
        $oldData->nama = $request->get('nama');
        $oldData->no_hp_1 = $request->get('no_hp_1');
        $oldData->no_hp_2 = $request->get('no_hp_2');
        $oldData->email = $request->get('email');
        $oldData->alamat = $request->get('alamat');
        $oldData->id_kecamatan = $request->get('id_kecamatan');
        $oldData->id_kota = $request->get('id_kota');
        $oldData->id_provinsi = $request->get('id_provinsi');
        $oldData->kodepos = $request->get('kodepos');
        $oldData->update();

        return redirect('/admin/setting-web')->with('success', 'Data berhasil diperbaharui');
    }
}
