<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class AdmLapProdukController extends Controller
{
    public function index(Request $resquest)
    {
        $firstDay = date('Y-m-01');
        $lastDay = date('Y-m-t');

        if (request()->isMethod('post')) {
            $firstDay = explode(" - ", $resquest->get('tgl_transaksi'))[0];
            $lastDay = explode(" - ", $resquest->get('tgl_transaksi'))[1];
        }

        $products = DB::table('produk')->get();
        $arrProducts =[];
        foreach ($products as $key => $value) {
            $arrProducts[$value->kode] = $value->stok;
        }

        $datas = DB::table('transaksi_dtl')
            ->leftJoin('transaksi', 'transaksi.no_invoice', '=', 'transaksi_dtl.no_invoice')
            ->leftJoin('produk', 'transaksi_dtl.kode_produk', '=', 'produk.kode')
            ->select('produk.kode', 'produk.nama', DB::raw('SUM(produk.stok) as stok,
                    SUM(transaksi_dtl.qty) as terjual, ifnull(sum(transaksi_dtl.jumlah),0) -
                    produk.harga_modal *
                    ifnull(sum(transaksi_dtl.qty),0) AS keuntungan'))
            ->whereBetween('transaksi.tgl_transaksi', [$firstDay, $lastDay])
            ->groupBy('produk.kode', 'produk.nama', 'produk.harga_modal')
            ->orderBy('terjual', 'DESC')
            ->get();

        return view('admin.store.laporan.produk.index', \compact('datas', 'firstDay', 'lastDay', 'arrProducts'));
    }
}
