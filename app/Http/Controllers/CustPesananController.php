<?php

namespace App\Http\Controllers;

use App\TransaksiByr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class CustPesananController extends Controller
{
    public function index($sttsUmum = 0)
    {
        $idCustomer = Session::get('id_customer');

        $listOrder = TransaksiByr::where('id_customer', '=', $idCustomer)
            ->orderBy('tgl_transaksi', 'DESC')
            ->paginate(10);

        $status = DB::table('stts_transaksi')->get();

        return view('customer.pesanan.index', \compact('listOrder', 'status', 'sttsUmum'));
    }

    public function detail($invoice)
    {
        $transaksiByr = TransaksiByr::with(['_transaksis', '_transaksis._details', '_transaksis._details._produk', '_transaksis._unit', '_transaksis._unit._city'])->where('no_invoice', $invoice)->first();

        return view('customer.pesanan.detail_v2', compact('transaksiByr'));
    }

    // public function detail($invoice)
    // {
    //     $myOrder = DB::table('transaksi_dtl as a')
    //             ->leftJoin('produk as b', 'a.kode_produk', '=', 'b.id')
    //             ->select('a.id', 'a.kode_produk', 'a.harga', 'a.qty', 'a.jumlah', 'b.nama', 'b.berat')
    //             ->where('no_invoice', '=', $invoice)
    //             ->get();

    //     $transaction = DB::table('transaksi')
    //             ->leftJoin('tb_ro_provinces', 'transaksi.id_provinsi', '=',  'tb_ro_provinces.province_id')
    //             ->leftJoin('tb_ro_cities', 'transaksi.id_kota', '=',  'tb_ro_cities.city_id')
    //             ->leftJoin('tb_ro_subdistricts', 'transaksi.id_kecamatan', '=',  'tb_ro_subdistricts.subdistrict_id')
    //             ->leftJoin('stts_transaksi', 'transaksi.stts_umum', '=', 'stts_transaksi.kode')
    //             ->select('transaksi.*',
    //                     'stts_transaksi.nama as nm_stts_umum',
    //                     'tb_ro_provinces.province_name',
    //                     'tb_ro_cities.city_name', 'tb_ro_subdistricts.subdistrict_name')
    //             ->where('no_invoice', '=', $invoice)
    //             ->first();

    //     $dropship = DB::table('transaksi')
    //             ->leftJoin('tb_ro_provinces', 'transaksi.dropship_id_provinsi', '=',  'tb_ro_provinces.province_id')
    //             ->leftJoin('tb_ro_cities', 'transaksi.dropship_id_kota', '=',  'tb_ro_cities.city_id')
    //             ->leftJoin('tb_ro_subdistricts', 'transaksi.dropship_id_kecamatan', '=',  'tb_ro_subdistricts.subdistrict_id')
    //             ->select(
    //                     'transaksi.dropship_nama', 'transaksi.dropship_no_hp', 'transaksi.dropship_alamat',
    //                     'tb_ro_provinces.province_name',
    //                     'tb_ro_cities.city_name', 'tb_ro_subdistricts.subdistrict_name')
    //             ->where('no_invoice', '=', $invoice)
    //             ->first();

    //     return view('customer.pesanan.detail', \compact('myOrder', 'transaction', 'dropship'));
    // }

    public function konfirmasiSampai(Request $request)
    {
        $invoiceId = $request->get('no_invoice');
        $sttsUmum = $request->get('stts_umum');

        $update = DB::table('transaksi')
                ->where('no_invoice', '=', "$invoiceId")
                ->update(["stts_umum" => $sttsUmum]);

        return redirect('/customer/pesanan/detail/' . $invoiceId);
    }
}
