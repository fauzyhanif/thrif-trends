<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

use App\KategoriProduk;
use App\FotoProduk;
use App\Produk;

class CustProdukController extends Controller
{
    public function index($kategori = '*')
    {
        $categories = KategoriProduk::all();
        $products = Produk::where("is_aktif", "Y")->paginate(12);

        if ($kategori != '*') {
            $products = Produk::where('kategori_produk', '=', $kategori)
                        ->where("is_aktif", "Y")
                        ->paginate(12);
        }

        return view('customer.produk.index', compact('kategori', 'categories', 'products'));
    }

    public function search(Request $request)
    {
        $key = $request->get('key');
        if (request()->isMethod('post')) {
            Session::put('key_search_produk', $key);
        }

        $keys = Session::get('key_search_produk');

        $count_products = DB::table('produk')
                            ->where("nama", "like", "%$keys%")
                            ->where("is_aktif", "Y")
                            ->get();

        $countResult = count($count_products);
        
        $products = Produk::with('_unit', '_unit._city')->where("nama", "like", "%$keys%")->where("is_aktif", "Y")->paginate(12);


        return view('customer.produk.search', compact('products', 'keys', 'countResult'));
    }

    public function detail($id)
    {
        $data = Produk::find($id);
        $dtFoto = FotoProduk::where("id_produk", "=", $id)->get();

        return view('customer.produk.detail', compact('data', 'dtFoto'));
    }
}
