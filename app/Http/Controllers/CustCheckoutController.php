<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Ixudra\Curl\Facades\Curl;

use App\GnrtInvoice;
use App\Keranjang;
use App\Transaksi;
use App\TransaksiByr;
use App\TransaksiDtl;

class CustCheckoutController extends Controller
{

    public function index()
    {
        $idCustomer = Session::get('id_customer');

        $units =  DB::table('keranjang as a')
                ->leftJoin('produk as b', 'a.kode_produk', '=', 'b.id')
                ->leftJoin('sys_ref_setting as c', 'b.id_unit', '=', 'c.id_unit')
                ->leftJoin('tb_ro_cities as d', 'c.id_kota', '=', 'd.city_id')
                ->select('c.id_unit', 'c.id_kecamatan', 'c.nama', 'd.city_name', DB::raw('SUM(b.berat) AS total_weight'))
                ->groupBy('c.id_unit', 'c.id_kecamatan', 'c.nama', 'd.city_name')
                ->get();

        $myCart = DB::table('keranjang as a')
                ->leftJoin('produk as b', 'a.kode_produk', '=', 'b.id')
                ->select('a.id', 'a.kode_produk', 'a.harga', 'a.qty', 'a.jumlah', 'b.nama', 'b.berat', 'b.id_unit')
                ->where('id_customer', '=', $idCustomer)
                ->get();

        $myProfile = DB::table('customer')
                ->leftJoin('tb_ro_provinces', 'customer.id_provinsi', '=',  'tb_ro_provinces.province_id')
                ->leftJoin('tb_ro_cities', 'customer.id_kota', '=',  'tb_ro_cities.city_id')
                ->leftJoin('tb_ro_subdistricts', 'customer.id_kecamatan', '=',  'tb_ro_subdistricts.subdistrict_id')
                ->select('customer.nama', 'customer.no_hp', 'customer.id_kecamatan',
                        'customer.alamat', 'tb_ro_provinces.province_name',
                        'tb_ro_cities.city_name', 'tb_ro_subdistricts.subdistrict_name')
                ->where('customer.id', '=', $idCustomer)
                ->first();

        $costs = [];
        foreach ($units as $unit) {
            $costs[$unit->id_unit] = $this->getCosts($unit->id_kecamatan, $myProfile->id_kecamatan, $unit->total_weight);
        }

        $provinces = DB::table('tb_ro_provinces')
            ->orderBy('province_name', 'ASC')
            ->get();

        return view('customer.checkout.index_v2', \compact('myCart', 'myProfile', 'provinces', 'units', 'costs'));
    }

    public function getCosts($origin, $destination, $weight)
    {

        // get costs
        $response = Curl::to('https://pro.rajaongkir.com/api/cost')
            ->withHeader("key: 4ef712a6359604345f2d87ae27ce9396")
            ->withData([
                "origin" => "$origin",
                "originType" => "subdistrict",
                "destination" => "$destination",
                "destinationType" => "subdistrict",
                "weight" => "$weight",
                "courier" => "jne:jnt"
            ])
            ->asJson()
            ->post();

        return $response->rajaongkir->results;
    }

    public function getCostDropshipper(Request $request)
    {
        $idKecamatan = $request->get("id_kecamatan");
        $weight = $request->get("weight");

        // get origin unit
        $unit = DB::table('sys_ref_setting')->where('id_unit', '=', 1)->first();

        // get costs
        $response = Curl::to('https://pro.rajaongkir.com/api/cost')
            ->withHeader("key: 4ef712a6359604345f2d87ae27ce9396")
            ->withData([
                "origin" => "$unit->id_kecamatan",
                "originType" => "subdistrict",
                "destination" => "$idKecamatan",
                "destinationType" => "subdistrict",
                "weight" => "$weight",
                "courier" => "jne:jnt"
            ])
            ->asJson()
            ->post();

        $costs = $response->rajaongkir->results;
        return view('customer.checkout.costs', \compact('costs'));
    }

    public function createOrder(Request $request)
    {
        DB::beginTransaction();
        try {
            $idCustomer = Session::get('id_customer');
            $profile = $this->getProfile($idCustomer);

            // create transaksi_byr
            $parameterTransaksiByr = [
                'id_customer' => $idCustomer,
                'tgl_transaksi' => date('Y-m-d'),
                'no_invoice' => $this->getInvoiceId('INV'),
                'total_transaksi' => $request->total_transaction,
                'status' => 0
            ];

            $transaksiByr = TransaksiByr::create($parameterTransaksiByr);

            foreach ($request->units as $unit) {
                $parameterTransaksi = [
                    'id_transaksi_byr' => $transaksiByr->id,
                    'id_unit' => $unit,
                    'tgl_transaksi' => date('Y-m-d'),
                    'no_invoice' => $this->getInvoiceId('INV'),
                    'id_customer' => $idCustomer,
                    'nama' => $profile->nama,
                    'no_hp' => $profile->no_hp,
                    'alamat' => $profile->alamat,
                    'id_kecamatan' => $profile->id_kecamatan,
                    'id_kota' => $profile->id_kota,
                    'id_provinsi' => $profile->id_provinsi,
                    'logistik_name' => explode(" * ", $request->get('customRadio'.$unit))[0],
                    'logistik_service' => explode(" * ", $request->get('customRadio'.$unit))[1],
                    'logistik_day' => explode(" * ", $request->get('customRadio'.$unit))[2],
                    'ttl_berat' => $request->get('subtotal_weight_'.$unit),
                    'ttl_ongkir' => explode(" * ", $request->get('customRadio'.$unit))[3],
                    'ttl_qty' => $request->get('subtotal_item_'.$unit),
                    'ttl_belanja' => $request->get('subtotal_product_'.$unit),
                    'ttl_transaksi' => $request->get('subtotal_product_'.$unit) + explode(" * ", $request->get('customRadio'.$unit))[3],
                    'stts_umum' => 0,
                    'stts_bayar' => 0,
                    'stts_batal' => 0
                ];

                $transaksi = Transaksi::create($parameterTransaksi);

                $this->saveTransactionDtl($transaksi->no_invoice, $idCustomer, $unit);
            }

            $this->emptyCart($idCustomer);
            
            DB::commit();
            return json_encode($transaksiByr->no_invoice);
        } catch (\Throwable $th) {
            DB::rollBack();
            throw new \Exception($th->getMessage(), 400);
        }
    }


    public function getInvoiceId($type = 'INV', $length = 5)
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[random_int(0, $charactersLength - 1)];
        }
    
        return $type.date('Ymd').$randomString;
    }

    public function getAmount($idCustomer)
    {
        $amount = DB::table('keranjang as a')
            ->leftJoin('produk as b', 'a.kode_produk', '=', 'b.id')
            ->where('id_customer', '=', $idCustomer)
            ->selectRaw('sum(a.qty * b.berat) as berat, sum(a.qty) as qty, sum(a.jumlah) as jumlah')
            ->first();

        $result = [
            "ttl_berat" => $amount->berat,
            "ttl_qty" => $amount->qty,
            "ttl_belanja" => $amount->jumlah
        ];

        return $result;
    }

    public function getProfile($idCustomer)
    {
        $profile = DB::table('customer')->where('id', '=', $idCustomer)->first();

        return $profile;
    }

    public function saveTransactionDtl($invoiceId, $idCustomer, $idUnit)
    {
        $goods = DB::table('keranjang')->where('id_customer', '=', $idCustomer)->where('id_unit', $idUnit)->get();

        foreach ($goods as $key => $value) {
            $new = new TransaksiDtl();
            $new->no_invoice = $invoiceId;
            $new->kode_produk = $value->kode_produk;
            $new->qty = $value->qty;
            $new->harga = $value->harga;
            $new->jumlah = $value->jumlah;
            $new->save();
        }
    }

    public function emptyCart($idCustomer)
    {
        $cart = Keranjang::where('id_customer', '=', $idCustomer)->delete();
    }

    public function pembayaran($invoice)
    {
        $invoice = DB::table('transaksi_byr')
            ->where('no_invoice', '=', "$invoice")
            ->selectRaw('total_transaksi, no_invoice')
            ->first();

        return view('customer.checkout.pembayaran', compact('invoice'));
    }

    public function waitingRequestJtr($invoice)
    {
        return view('customer.checkout.waitingRequestJtr', \compact('invoice'));
    }
}

/*
Daftar kurir
pos, tiki, jne, pcp, esl, rpx, pandu, wahana, jnt, pahala, cahaya, sap, jet, indah, dse, slis, first, ncs, dan star
*/
