<?php

namespace App\Http\Controllers;

use App\DressUp;
use App\Keranjang;
use App\MatchingScore;
use App\Produk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class CustDressUpController extends Controller
{
    public function index()
    {
        $upperClothes = Produk::whereHas('_kategori_produk', function($query){
                $query->where('jenis', 'atasan');
            })
            ->orderBy('nama')
            ->limit(5)
            ->get();

        $bottomClothes = Produk::whereHas('_kategori_produk', function($query){
                $query->where('jenis', 'bawahan');
            })
            ->orderBy('nama')
            ->limit(5)
            ->get();

        return view('customer.dress_up.index', compact('upperClothes', 'bottomClothes'));
    }

    public function create(Request $request)
    {
        $idCustomer = Session::get('id_customer');
        $produk = Produk::with(['_kategori_produk'])->findOrFail($request->id_produk);
        $checkExists = DressUp::where('jenis_produk', $produk->_kategori_produk->jenis)->where('id_customer', $idCustomer);
        if ($checkExists->count() > 0) {
            $checkExists->delete();
        }

        $dressUpRequest = [
            'id_produk' => $request->id_produk,
            'jenis_produk' => $produk->_kategori_produk->jenis,
            'id_customer' => $idCustomer
        ];

        DressUp::create($dressUpRequest);

        Keranjang::where('id_customer', $idCustomer)->update(['is_dress_up' => '1']);

        return json_encode(true);
    }

    public function get()
    {
        $idCustomer = Session::get('id_customer');
        $upperClothes = DressUp::with(['_produk'])->where('id_customer', $idCustomer)->where('jenis_produk', 'atasan')->first();
        $bottomClothes = DressUp::with(['_produk'])->where('id_customer', $idCustomer)->where('jenis_produk', 'bawahan')->first();
        $matchingScore = $this->matchingScore($upperClothes, $bottomClothes);
        return view('customer.dress_up.get', compact('upperClothes', 'bottomClothes', 'matchingScore'));
    }

    public function matchingScore($upperClothes = null, $bottomClothes = null)
    {
        $score = 0;
        $productIdUpper = ($upperClothes) ? $upperClothes->id_produk : null;
        $productIdBottom = ($bottomClothes) ? $bottomClothes->id_produk : null;

        $exists = MatchingScore::where('id_produk_atasan', $productIdUpper)->where('id_produk_bawahan', $productIdBottom)->first();
        
        if ($exists) {
            $score = $exists->score;
        } else {
            if ($upperClothes && $bottomClothes) {
                if ($upperClothes->_produk->gender == $bottomClothes->_produk->gender) {
                    $score = rand(70, 90);
                } else {
                    $score = rand(1, 30);
                }

                $matchingScoreRequest = [
                    'id_produk_atasan' => $upperClothes->id_produk,
                    'id_produk_bawahan' => $bottomClothes->id_produk,
                    'score' => $score
                ];

                MatchingScore::create($matchingScoreRequest);
            }
        }

        return $score;
    }

}
