<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class AdmLapCustomerController extends Controller
{
    public function index(Request $resquest)
    {
        $firstDay = date('Y-m-01');
        $lastDay = date('Y-m-t');

        if (request()->isMethod('post')) {
            $firstDay = explode(" - ", $resquest->get('tgl_transaksi'))[0];
            $lastDay = explode(" - ", $resquest->get('tgl_transaksi'))[1];
        }

        $datas = DB::table('transaksi')
                ->leftJoin('customer', 'transaksi.id_customer', '=', 'customer.id')
                ->leftJoin('tb_ro_cities', 'customer.id_kota', '=',  'tb_ro_cities.city_id')
                ->select('customer.nama', 'tb_ro_cities.city_name',
                        DB::raw('SUM(transaksi.ttl_belanja) as jml_omset,
                        SUM(transaksi.ttl_qty) as jml_qty
                        '))
                ->where('transaksi.stts_umum', '!=', '0')
                ->whereBetween('transaksi.tgl_transaksi', [$firstDay, $lastDay])
                ->orderBy('jml_omset', 'DESC')
                ->groupBy('transaksi.id_customer', 'customer.nama', 'tb_ro_cities.city_name')
                ->get();

        return view('admin.store.laporan.customer.index', \compact('datas', 'firstDay', 'lastDay'));
    }
}
