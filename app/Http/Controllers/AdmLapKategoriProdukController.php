<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class AdmLapKategoriProdukController extends Controller
{
    public function index(Request $resquest)
    {
        $firstDay = date('Y-m-01');
        $lastDay = date('Y-m-t');

        if (request()->isMethod('post')) {
            $firstDay = explode(" - ", $resquest->get('tgl_transaksi'))[0];
            $lastDay = explode(" - ", $resquest->get('tgl_transaksi'))[1];
        }

        $datas = DB::table('kategori_produk')
            ->leftJoin('produk', 'kategori_produk.id', '=', 'produk.kategori_produk')
            ->leftJoin('transaksi_dtl', 'produk.kode', '=', 'transaksi_dtl.kode_produk')
            ->leftJoin('transaksi', 'transaksi_dtl.no_invoice', '=', 'transaksi.no_invoice')
            ->selectRaw('
                transaksi_dtl.kode_produk,
                kategori_produk.nama,
                sum(produk.stok) as stok,
                sum(transaksi_dtl.qty) as terjual,
                sum(transaksi_dtl.jumlah -
                    produk.harga_modal *
                    transaksi_dtl.qty) AS keuntungan
            ')
            ->whereBetween('transaksi.created_at', [$firstDay, $lastDay])
            ->orderBy('terjual', 'DESC')
            ->groupBy('transaksi_dtl.kode_produk')
            ->get();


        return view('admin.store.laporan.kategoriProduk.index', \compact('datas', 'firstDay', 'lastDay'));
    }

    public function detail($id, $firstDay, $lastDay)
    {
        $category = DB::table('kategori_produk')->where('id', '=', $id)->first();
        $datas = DB::table('transaksi_dtl')
            ->leftJoin('transaksi', 'transaksi.no_invoice', '=', 'transaksi_dtl.no_invoice')
            ->leftJoin('produk', 'transaksi_dtl.kode_produk', '=', 'produk.kode')
            ->select('produk.nama', DB::raw('SUM(produk.stok) as stok,
                    SUM(transaksi_dtl.qty) as terjual, ifnull(sum(transaksi_dtl.jumlah),0) -
                    produk.harga_modal *
                    ifnull(sum(transaksi_dtl.qty),0) AS keuntungan'))
            ->where('produk.kategori_produk', '=', $id)
            ->whereBetween('transaksi.tgl_transaksi', [$firstDay, $lastDay])
            ->groupBy('produk.kode', 'produk.nama', 'produk.harga_modal')
            ->get();

            return view('admin.store.laporan.kategoriProduk.detail', \compact('category', 'datas', 'firstDay', 'lastDay'));
    }
}
