<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

use App\Keranjang;

class CustKeranjangController extends Controller
{
    public function index()
    {
        $idCustomer = Session::get('id_customer');
        $countMyCart = 0;

        $myCart = DB::table('keranjang as a')
            ->leftJoin('produk as b', 'a.kode_produk', '=', 'b.id')
            ->select('a.id', 'a.kode_produk', 'a.harga', 'a.qty', 'a.jumlah', 'b.nama')
            ->where('id_customer', '=', $idCustomer)
            ->get();

        $countMyCart = count($myCart);
        $checkDressUp = Keranjang::where('id_customer', $idCustomer)->where('is_dress_up', '0')->count();

        if ($checkDressUp > 0) {
            return redirect('customer/dress-up');
        }

        return view('customer.keranjang.index', compact('myCart', 'countMyCart'));
    }

    public function addToCart(Request $request)
    {
        $response = [];
        if (Session::has('id_customer')) {
            $kodeProduk = $request->get('kode_produk');
            $idCustomer = Session::get('id_customer');

            // get harga from master produk
            $dtProduk = DB::table('produk')
                        ->where('id', '=', $kodeProduk)
                        ->first();

            // cek apakah produk ini sudah ada di keranjang
            $cek = DB::table('keranjang')
                    ->where('kode_produk', '=', $kodeProduk)
                    ->where('id_customer', '=', $idCustomer)
                    ->first();

            if ($cek === null) {
                // save
                $keranjangBaru = new Keranjang();
                $keranjangBaru->id_unit = $dtProduk->id_unit;
                $keranjangBaru->id_customer = $idCustomer;
                $keranjangBaru->kode_produk = $kodeProduk;
                $keranjangBaru->qty = 1;
                $keranjangBaru->harga = $dtProduk->harga_jual;
                $keranjangBaru->jumlah = $dtProduk->harga_jual;
                $keranjangBaru->save();
            } else {
                $keranjangLama = Keranjang::where('kode_produk', '=', $kodeProduk)->firstOrFail();
                $keranjangLama->qty = $cek->qty + 1;
                $keranjangLama->jumlah = $dtProduk->harga_jual * ($cek->qty + 1);
                $keranjangLama->update();
            }

            $response = [
                "status" => "success",
                "text" => "Ditambahkan ke keranjang"
            ];
        } else {
            $response = [
                "status" => "danger",
                "text" => "Anda harus login ulang."
            ];
        }

        return json_encode($response);
    }

    public function ReloadHeaderCountCart()
    {
        $idCustomer = Session::get('id_customer');
        $countMyCart = 0;
        $myCart = DB::table('keranjang')->where('id_customer', '=', $idCustomer)->get();

        $countMyCart = count($myCart);

        return $countMyCart;
    }

    public function ReloadHeaderListCart()
    {
        $idCustomer = Session::get('id_customer');
        $countMyCart = 0;

        $myCart = DB::table('keranjang as a')
                ->leftJoin('produk as b', 'a.kode_produk', '=', 'b.id')
                ->select('a.harga', 'b.nama', 'b.thumbnail')
                ->where('id_customer', '=', $idCustomer)
                ->get();

        return $myCart;
    }

    public function updateDataItem(Request $request)
    {
        $id = $request->get('id');
        $qty = $request->get('qty');
        $jumlah = $request->get('jumlah');

        $cart = Keranjang::find($id);
        $cart->qty = $qty;
        $cart->jumlah = $jumlah;
        $cart->update();
    }

    public function removeDataItem(Request $request)
    {
        $id = $request->get('id');

        $cart = Keranjang::find($id);
        $cart->delete();
    }
}
