<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent;
use Illuminate\Support\Facades\Session;
use DataTables;

use App\Transaksi;
use App\SttsTransaksi;

class AdmTransaksiController extends Controller
{
    public function index($idStatus = '*')
    {
        $total = DB::table('transaksi')
                ->selectRaw('count(*) as total,
                            sum(if(stts_umum = 0,1,0)) as belum_bayar,
                            sum(if(stts_umum = 1,1,0)) as dikemas,
                            sum(if(stts_umum = 2,1,0)) as dikirim,
                            sum(if(stts_umum = 3,1,0)) as selesai
                            ')
                ->where('id_unit', Session::get('id_unit'))
                ->first();

        $status = DB::table('stts_transaksi')->get();
        return view('admin.store.transaksi.index', \compact('total', 'status', 'idStatus'));
    }

    public function detail($invoice)
    {
        $orderList = DB::table('transaksi_dtl as a')
                ->leftJoin('produk as b', 'a.kode_produk', '=', 'b.kode')
                ->select('a.id', 'a.kode_produk', 'a.harga', 'a.qty', 'a.jumlah', 'b.nama', 'b.berat')
                ->where('no_invoice', '=', $invoice)
                ->get();

        $orderInformation = DB::table('transaksi')
                ->leftJoin('tb_ro_provinces', 'transaksi.id_provinsi', '=',  'tb_ro_provinces.province_id')
                ->leftJoin('tb_ro_cities', 'transaksi.id_kota', '=',  'tb_ro_cities.city_id')
                ->leftJoin('tb_ro_subdistricts', 'transaksi.id_kecamatan', '=',  'tb_ro_subdistricts.subdistrict_id')
                ->leftJoin('stts_transaksi', 'transaksi.stts_umum', '=',  'stts_transaksi.kode')
                ->select('transaksi.*',
                        'tb_ro_provinces.province_name',
                        'tb_ro_cities.city_name',
                        'tb_ro_subdistricts.subdistrict_name',
                        'stts_transaksi.nama as nm_status')
                ->where('no_invoice', '=', $invoice)
                ->first();

        $dropship = DB::table('transaksi')
                ->leftJoin('tb_ro_provinces', 'transaksi.dropship_id_provinsi', '=',  'tb_ro_provinces.province_id')
                ->leftJoin('tb_ro_cities', 'transaksi.dropship_id_kota', '=',  'tb_ro_cities.city_id')
                ->leftJoin('tb_ro_subdistricts', 'transaksi.dropship_id_kecamatan', '=',  'tb_ro_subdistricts.subdistrict_id')
                ->select(
                        'transaksi.dropship_nama', 'transaksi.dropship_no_hp', 'transaksi.dropship_alamat',
                        'tb_ro_provinces.province_name',
                        'tb_ro_cities.city_name', 'tb_ro_subdistricts.subdistrict_name')
                ->where('no_invoice', '=', $invoice)
                ->first();

        $sttsTransaksi = DB::table('stts_transaksi')->get();
        return view('admin.store.transaksi.detail', \compact('orderInformation', 'orderList', 'sttsTransaksi', 'dropship'));
    }

    public function getTransactionByMonth($idStatus = '*')
    {
        $year = date('Y');
        $month = date('m');

        if ($idStatus == '*') {
            $datas = DB::table('transaksi as a')
                ->leftJoin('stts_transaksi as b', 'a.stts_umum', '=', 'b.kode')
                ->select('a.no_invoice', 'a.is_dropshipper', 'a.nama', 'a.no_hp',
                        'a.tgl_transaksi', 'a.ttl_transaksi', 'a.stts_umum', 'b.nama as status',
                        'a.request_jtr', 'a.konfirmasi_jtr')
                ->where('a.id_unit', Session::get('id_unit'))
                // ->whereYear('tgl_transaksi', '=', $year)
                ->orderBy('no_invoice', 'DESC');
        } else {
            $datas = DB::table('transaksi as a')
                ->leftJoin('stts_transaksi as b', 'a.stts_umum', '=', 'b.kode')
                ->select('a.no_invoice', 'a.is_dropshipper', 'a.nama', 'a.no_hp',
                        'a.tgl_transaksi', 'a.ttl_transaksi', 'a.stts_umum', 'b.nama as status',
                        'a.request_jtr', 'a.konfirmasi_jtr')
                // ->whereYear('tgl_transaksi', '=', $year)
                ->where('stts_umum', '=', $idStatus)
                ->where('a.id_unit', Session::get('id_unit'))
                ->orderBy('no_invoice', 'DESC');
        }

        return DataTables::of($datas)
            ->editColumn('tgl_transaksi', function($datas){
                $helper = new HelpersController();
                $result = $helper->konversiTgl($datas->tgl_transaksi, 'T');
                return $result;
            })
            ->editColumn('ttl_transaksi', function($datas){
                $helper = new HelpersController();
                $result = $helper->formatRupiah($datas->ttl_transaksi);
                return $result;
            })
            ->addColumn('nama_edited' , function($datas){
                $html = $datas->nama . "<br>";
                if ($datas->is_dropshipper == 'Y') {
                    $html .= "<label class='badge badge-warning'>Dropship</label>";
                }

                if ($datas->request_jtr == 'Y') {
                    if ($datas->konfirmasi_jtr == 'Y') {
                        $html .= " <label class='badge badge-info'>JTR sudah dikonfirmasi</label>";
                    } else {
                        $html .= " <label class='badge badge-info'>Request JTR</label>";
                    }
                }

                return $html;
            })
            ->addColumn('badge_status' , function($datas){
                $type = "secondary";
                $icon = "<i class='fas fa-spinner'></i>";

                if ($datas->stts_umum == '1') {
                    $type = "warning";
                    $icon = "<i class='fas fa-cube'></i>";
                } elseif ($datas->stts_umum == '2') {
                    $type = "info";
                    $icon = "<i class='fas fa-truck'></i>";
                } elseif ($datas->stts_umum == '3') {
                    $type = "success";
                    $icon = "<i class='fas fa-check-circle'></i>";
                }

                $label = "<label class='badge badge-$type'>$icon &nbsp; $datas->status</label>";

                return $label;
            })
            ->addColumn('btn_action', function($datas){
                $btn = "<a href='" . url('/admin/transaksi/detail', $datas->no_invoice) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Tanggapi Pesanan";
                $btn .= "</a> ";
                $btn .= "<a href='" . url('/admin/transaksi/print-invoice', $datas->no_invoice) . "' target='_blank' class='btn btn-secondary btn-xs'>";
                $btn .= "<i class='fas fa-print'></i> Print Invoice";
                $btn .= "</a>";

                return $btn;
            })
            ->rawColumns(['badge_status', 'btn_action', 'nama_edited'])
            ->toJson();
    }

    public function changeStatusTransaction(Request $request, $invoiceId)
    {
        $stts_umum = $request->get('stts_umum');
        $resi = $request->get('resi');

        $update = DB::table('transaksi')
                ->where('no_invoice', '=', "$invoiceId")
                ->update(["stts_umum" => $stts_umum, "resi" => "$resi"]);

        // check subrtaction stock
        $check = DB::table('transaksi')
                ->select('subtraction_stock')
                ->where('no_invoice', '=', "$invoiceId")
                ->first();

        // do subtraction stock
        if ($stts_umum == '1' && $check->subtraction_stock == '0') {
            $this->subtractionStock($invoiceId);
        }

        return redirect('/admin/transaksi/detail/' . $invoiceId)->with('success', 'Status transaksi berhasil diperbaharui');
    }

    public function inputOngkirJtr(Request $request, $invoiceId)
    {
        $konfirmasi_jtr = $request->get('konfirmasi_jtr');
        $logistik_name = $request->get('logistik_name');
        $logistik_service = $request->get('logistik_service');
        $logistik_day = $request->get('logistik_day');
        $ttl_ongkir = $request->get('ttl_ongkir');

        $update = DB::table('transaksi')
                ->where('no_invoice', '=', "$invoiceId")
                ->update([
                    "konfirmasi_jtr" => $konfirmasi_jtr,
                    "logistik_name" => $logistik_name,
                    "logistik_service" => $logistik_service,
                    "logistik_day" => $logistik_day,
                    "ttl_ongkir" => $ttl_ongkir,
                    "ttl_transaksi" => DB::raw("ttl_belanja + $ttl_ongkir"),
                ]);

        return redirect('/admin/transaksi/detail/' . $invoiceId)->with('success', 'Ongkir JTR berhasil disimpan');
    }

    public function subtractionStock($invoiceId)
    {
        // get product list from transaction detail
        $products = DB::table('transaksi_dtl')
                    ->select('kode_produk', 'qty')
                    ->where('no_invoice', '=', "$invoiceId")
                    ->get();

        // reduction in product stock
        foreach ($products as $key => $value) {
            $stock = DB::table('produk')
                    ->where('kode', '=', "$value->kode_produk")
                    ->update(["stok" => DB::raw("stok - $value->qty")]);
        }

        // change status subtraction stock
        $changeStatusSubtraction = DB::table('transaksi')
                                ->where('no_invoice', '=', "$invoiceId")
                                ->update(["subtraction_stock" => 1]);

    }

    public function printInvoice($invoice)
    {
        $info = DB::table('transaksi')
                ->leftJoin('tb_ro_provinces', 'transaksi.id_provinsi', '=',  'tb_ro_provinces.province_id')
                ->leftJoin('tb_ro_cities', 'transaksi.id_kota', '=',  'tb_ro_cities.city_id')
                ->leftJoin('tb_ro_subdistricts', 'transaksi.id_kecamatan', '=',  'tb_ro_subdistricts.subdistrict_id')
                ->leftJoin('stts_transaksi', 'transaksi.stts_umum', '=',  'stts_transaksi.kode')
                ->select('transaksi.*',
                        'tb_ro_provinces.province_name',
                        'tb_ro_cities.city_name',
                        'tb_ro_subdistricts.subdistrict_name',
                        'stts_transaksi.nama as nm_status')
                ->where('no_invoice', '=', $invoice)
                ->first();

        $dropship = DB::table('transaksi')
                ->leftJoin('tb_ro_provinces', 'transaksi.dropship_id_provinsi', '=',  'tb_ro_provinces.province_id')
                ->leftJoin('tb_ro_cities', 'transaksi.dropship_id_kota', '=',  'tb_ro_cities.city_id')
                ->leftJoin('tb_ro_subdistricts', 'transaksi.dropship_id_kecamatan', '=',  'tb_ro_subdistricts.subdistrict_id')
                ->select(
                        'transaksi.dropship_nama', 'transaksi.dropship_no_hp', 'transaksi.dropship_alamat',
                        'tb_ro_provinces.province_name',
                        'tb_ro_cities.city_name', 'tb_ro_subdistricts.subdistrict_name')
                ->where('no_invoice', '=', $invoice)
                ->first();

        $orderList = DB::table('transaksi_dtl as a')
                ->leftJoin('produk as b', 'a.kode_produk', '=', 'b.kode')
                ->select('a.id', 'a.kode_produk', 'a.harga', 'a.qty', 'a.jumlah', 'b.nama', 'b.berat')
                ->where('no_invoice', '=', $invoice)
                ->get();

        return view('admin.store.transaksi.printInvoice', compact('info', 'orderList', 'dropship'));
    }

    //////////////////////////////////////
    ///////* Print Invoice Banyak *///////
    //////////////////////////////////////

    public function printInvoiceBanyak($idStatus = '*')
    {
        $status = DB::table('stts_transaksi')->get();
        return view('admin/mbc.transaksi.printInvoiceBanyak.index', \compact('status', 'idStatus'));
    }
    public function getTransactionForPrint($idStatus = '*')
    {
        $year = date('Y');
        $month = date('m');

        if ($idStatus == '*') {
            $datas = DB::table('transaksi as a')
                ->leftJoin('stts_transaksi as b', 'a.stts_umum', '=', 'b.kode')
                ->select('a.no_invoice', 'a.is_dropshipper', 'a.nama', 'a.no_hp',
                        'a.tgl_transaksi', 'a.ttl_transaksi', 'a.stts_umum', 'b.nama as status',
                        'a.request_jtr', 'a.konfirmasi_jtr')
                // ->whereYear('tgl_transaksi', '=', $year)
                ->orderBy('no_invoice', 'DESC');
        } else {
            $datas = DB::table('transaksi as a')
                ->leftJoin('stts_transaksi as b', 'a.stts_umum', '=', 'b.kode')
                ->select('a.no_invoice', 'a.is_dropshipper', 'a.nama', 'a.no_hp',
                        'a.tgl_transaksi', 'a.ttl_transaksi', 'a.stts_umum', 'b.nama as status',
                        'a.request_jtr', 'a.konfirmasi_jtr')
                // ->whereYear('tgl_transaksi', '=', $year)
                ->where('stts_umum', '=', $idStatus)
                ->orderBy('no_invoice', 'DESC');
        }

        return DataTables::of($datas)
            ->editColumn('tgl_transaksi', function($datas){
                $helper = new HelpersController();
                $result = $helper->konversiTgl($datas->tgl_transaksi, 'T');
                return $result;
            })
            ->editColumn('ttl_transaksi', function($datas){
                $helper = new HelpersController();
                $result = $helper->formatRupiah($datas->ttl_transaksi);
                return $result;
            })
            ->addColumn('nama_edited' , function($datas){
                $html = $datas->nama . "<br>";
                if ($datas->is_dropshipper == 'Y') {
                    $html .= "<label class='badge badge-warning'>Dropship</label>";
                }

                if ($datas->request_jtr == 'Y') {
                    if ($datas->konfirmasi_jtr == 'Y') {
                        $html .= " <label class='badge badge-info'>JTR sudah dikonfirmasi</label>";
                    } else {
                        $html .= " <label class='badge badge-info'>Request JTR</label>";
                    }
                }

                return $html;
            })
            ->addColumn('badge_status' , function($datas){
                $type = "secondary";
                $icon = "<i class='fas fa-spinner'></i>";

                if ($datas->stts_umum == '1') {
                    $type = "warning";
                    $icon = "<i class='fas fa-cube'></i>";
                } elseif ($datas->stts_umum == '2') {
                    $type = "info";
                    $icon = "<i class='fas fa-truck'></i>";
                } elseif ($datas->stts_umum == '3') {
                    $type = "success";
                    $icon = "<i class='fas fa-check-circle'></i>";
                }

                $label = "<label class='badge badge-$type'>$icon &nbsp; $datas->status</label>";

                return $label;
            })
            ->addColumn('btn_action', function($datas){
                $checkbox = "<input type='checkbox' class='text-center' name='no_invoice[]' value='$datas->no_invoice'>";
                return $checkbox;
            })
            ->rawColumns(['badge_status', 'btn_action', 'nama_edited'])
            ->toJson();
    }

    public function doPrintInvoiceBanyak(Request $request)
    {
        $orders = DB::table('transaksi')
                ->leftJoin('tb_ro_provinces as gnrlj_province', 'transaksi.id_provinsi', '=',  'gnrlj_province.province_id')
                ->leftJoin('tb_ro_cities as gnrlj_city', 'transaksi.id_kota', '=',  'gnrlj_city.city_id')
                ->leftJoin('tb_ro_subdistricts as gnrlj_subdistrict', 'transaksi.id_kecamatan', '=',  'gnrlj_subdistrict.subdistrict_id')
                ->leftJoin('tb_ro_provinces as drpj_province', 'transaksi.dropship_id_provinsi', '=',  'drpj_province.province_id')
                ->leftJoin('tb_ro_cities as drpj_city', 'transaksi.dropship_id_kota', '=',  'drpj_city.city_id')
                ->leftJoin('tb_ro_subdistricts as drpj_subdistrict', 'transaksi.dropship_id_kecamatan', '=',  'drpj_subdistrict.subdistrict_id')
                ->leftJoin('stts_transaksi', 'transaksi.stts_umum', '=',  'stts_transaksi.kode')
                ->select('transaksi.*',
                        'gnrlj_province.province_name as gnrl_province',
                        'gnrlj_city.city_name as gnrl_city',
                        'gnrlj_subdistrict.subdistrict_name as gnrl_subdistrict',
                        'drpj_province.province_name as drp_province',
                        'drpj_city.city_name as drp_city',
                        'drpj_subdistrict.subdistrict_name as drp_subdistrict',
                        'stts_transaksi.nama as nm_status')
                ->whereIn('no_invoice', $request->get('no_invoice'))
                ->get();

        foreach ($orders as $keyi => $oi) {
            $oi->order_list = DB::table('transaksi_dtl as a')
                ->leftJoin('produk as b', 'a.kode_produk', '=', 'b.kode')
                ->select('a.id', 'a.no_invoice', 'a.kode_produk', 'a.harga', 'a.qty', 'a.jumlah', 'b.nama', 'b.berat')
                ->where('no_invoice', '=', $oi->no_invoice)
                ->get();
        }

        return view('admin/mbc.transaksi.printInvoiceBanyak.printInvoice', \compact('orders'));
    }
}
