<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HelpersController extends Controller
{
    public function cariKota($id)
    {
        $data = DB::table('tb_ro_cities')
            ->where('province_id', '=', $id)
            ->orderBy('city_name')
            ->get();

        return json_encode($data);
    }

    public function cariKecamatan($id)
    {
        $data = DB::table('tb_ro_subdistricts')
            ->where('city_id', '=', $id)
            ->orderBy('subdistrict_name')
            ->get();

        return json_encode($data);
    }

    public function konversiTgl($date, $date_format='')
    {
        $dayList = array(
            'Sunday'    => 'Minggu',
            'Monday'    => 'Senin',
            'Tuesday'   => 'Selasa',
            'Wednesday' => 'Rabu',
            'Thursday'  => 'Kamis',
            'Friday'    => 'Jumat',
            'Saturday'  => 'Sabtu'
        );

        $monthList = array(
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember'
        );

        $format_hari = date('l', strtotime($date));
        $format_tgl  = date('d', strtotime($date));
        $format_bln  = date('m', strtotime($date));
        $format_thn  = date('Y', strtotime($date));

        switch ($date_format) {
            case 'l':
                # Hari ex: Kamis
                $output = $dayList[$format_hari];
                break;
            case 'd':
                # Tanggal ex: 21
                $output = $format_tgl;
                break;
            case 'm':
                # Bulan ex: Januari
                $output = $monthList[$format_bln];
                break;
            case 'y':
                # Tahun ex: 2016
                $output = $format_thn;
                break;
            case 'T':
                # Tgl Lahir
                $output = $format_tgl . ' ' . $monthList[$format_bln] . ' ' . $format_thn;;
                break;
            default:
                # Hari, Tanggal-Bulan-Tahun ex: Rabu, 26-Juli-2016
                $output = $dayList[$format_hari] . ', ' . $format_tgl . ' ' . $monthList[$format_bln] . ' ' . $format_thn;
                break;
        }

        return $output;
    }

    public function formatRupiah($angka){
        $hasil = number_format($angka,2,',','.');
        return $hasil;
    }
}
