<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Mail\VerificationMail;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

use App\SysRefUser;
use App\SysRefUsergroup;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth.index');
    }

    public function doAuth(Request $request)
    {
        $request->validate([
            "username" => "required",
            "password" => "required"
        ]);

        $data_exist = SysRefUser::where('username', '=', $request->get('username'))->first();
        if ($data_exist === null) {
            return redirect('/auth')->withErrors(['Username salah.', '']);
        } else {
            if (!Hash::check($request->get('password'), $data_exist->password)) {
                return redirect('/auth')->withErrors(['Username atau password salah.', '']);
            } else {
                if ($data_exist->email_verified_at == null) {
                    return redirect('/auth')->withErrors(['Email belum terverifikasi.', '']);
                } else {
                    // set session
                    Session::put('nama', $data_exist->nama);
                    Session::put('username', $data_exist->username);
                    Session::put('usergroup', $data_exist->id_usergroup);
                    Session::put('jenjang', $data_exist->id_jenjang);
                    Session::put('id_unit', $data_exist->id_unit);

                    // cek jika distributor atau bukan
                    if ($data_exist->id_usergroup == ',3,') {
                        // jika distributor maka redirect profil santri
                        Session::put('id_customer', $data_exist->id_customer);
                        Session::put('usergroup_aktif', str_replace(",", "", $data_exist->id_usergroup));
                        return redirect('/customer/dashboard');
                    } else {
                        // jika admin maka redirect pilih usergroup
                        Session::put('usergroup_aktif', 1);
                        return redirect('/dashboard');
                    }
                }
            }
        }
    }

    public function pilihUsergroup(Request $request)
    {
        $sessionUsergroup = Session::get('usergroup');
        $dtUsergroup = SysRefUsergroup::where('id_usergroup', '!=', '3')->get();
        return view('auth.pilihUsergroup', compact('sessionUsergroup', 'dtUsergroup'));
    }

    public function setUsergroup(Request $request, $id)
    {
        Session::put('usergroup_aktif', $id);

        $nmUsergroup = SysRefUsergroup::where('id_usergroup', '=', $id)->first();
        Session::put('nama_usergroup_aktif', $nmUsergroup->nama);
        return redirect('/dashboard');
    }

    public function logout()
    {
        Session::flush();
        return redirect('/auth');
    }

    public function accessDenied()
    {
        return view('auth/accessDenied');
    }

    public function CheckSessionWithJs()
    {
        $session = Session::get('username');
        return json_encode($session);
    }

    public function loginUlang()
    {
        return view('customer.error.loginUlang');
    }

    public function registerPage()
    {
        $provinces = DB::table('tb_ro_provinces')->orderBy('province_name', 'ASC')->get();
        return view('auth.register', compact('provinces'));
    }

    public function doRegister(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'email' => 'required|unique:sys_ref_user,email',
            'username'=>'required|unique:sys_ref_user,username',
            'no_hp'=>'required',
            'alamat'=>'required',
            'id_kecamatan'=>'required',
            'id_kota'=>'required',
            'id_provinsi'=>'required'
        ]);

        $newData = new Customer();
        $newData->nama = $request->get('nama');
        $newData->no_hp = $request->get('no_hp');
        $newData->alamat = $request->get('alamat');
        $newData->id_kecamatan = $request->get('id_kecamatan');
        $newData->id_kota = $request->get('id_kota');
        $newData->id_provinsi = $request->get('id_provinsi');
        $newData->save();

        // add user login
        $token = Str::random(30);
        $request->request->add(['email_token' => $token]);
        $this->saveDataUser($request->all(), $newData->id);

        $this->sendEmailVerification($request->email, $token);

        // set session
        Session::put('nama', $request->nama);
        Session::put('username', $request->username);
        Session::put('usergroup', ',3,');
        Session::put('id_customer', $newData->id);
        Session::put('usergroup_aktif', 3);

        return redirect('/email-verification-page')->with(['email-for-verification' => $request->email]);
    }

    public function saveDataUser($data, $idCustomer)
    {
        $newData = new SysRefUser();
        $newData->nama = $data['nama'];
        $newData->username = $data['username'];
        $newData->email = $data['email'];
        $newData->password = Hash::make($data['password']);
        $newData->email_token = $data['email_token'];
        $newData->id_usergroup = ',3,';
        $newData->id_customer = $idCustomer;
        $newData->is_aktif = 'Y';
        $newData->save();
    }

    public function sendEmailVerification($email, $token)
    {
        $title = 'Verifikasi Email Thriftrens.com';
        Mail::to($email)->send(new VerificationMail($title, $email, $token));
    }

    public function resendEmailVerification(Request $request)
    {
        $email = $request->get('email');
        $token = Str::random(30);

        $user = SysRefUser::where('email', $email)->update(['email_token' => $token]);

        $title = 'Verifikasi Email Thriftrens.com';
        Mail::to($email)->send(new VerificationMail($title, $email, $token));
        return redirect('/email-verification-page')->with(['email-for-verification' => $email]);
    }

    public function emailverificationPage()
    {
        $email = Session::get('email-for-verification');
        return view('auth.verification_mail', compact('email'));
    }
    public function tokenVerification(Request $request)
    {
        $email = $request->get('email');
        $token = $request->get('token');

        $check = SysRefUser::where('email', $email)->where('email_token', $token);
        if ($check->count() > 0) {
            $check->update(['email_verified_at' => date('Y-m-d H:i:s')]);

            $data_exist = SysRefUser::where('email', $email)->first();
            if ($data_exist === null) {
                return redirect('/auth')->withErrors(['Username salah.', '']);
            } else {
                // set session
                Session::put('nama', $data_exist->nama);
                Session::put('username', $data_exist->username);
                Session::put('usergroup', $data_exist->id_usergroup);
                Session::put('jenjang', $data_exist->id_jenjang);
                Session::put('id_unit', $data_exist->id_unit);

                // cek jika distributor atau bukan
                if ($data_exist->id_usergroup == ',3,') {
                    // jika distributor maka redirect profil santri
                    Session::put('id_customer', $data_exist->id_customer);
                    Session::put('usergroup_aktif', str_replace(",", "", $data_exist->id_usergroup));
                    return redirect('/customer/dashboard');
                } else {
                    // jika admin maka redirect pilih usergroup
                    Session::put('usergroup_aktif', 1);
                    return redirect('/dashboard');
                }
            }
        } else {
            echo "<br><br><br><center>Verifikasi Gagal!<center>";
            die();
        }
    }
}
