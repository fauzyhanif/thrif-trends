<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ixudra\Curl\Facades\Curl;

use App\KategoriProduk;
use Illuminate\Support\Facades\Session;

class KategoriProdukController extends Controller
{
    public function index()
    {
        $datas = DB::table('kategori_produk')
            ->orderBy('nama', 'ASC')
            ->get();

        return view('admin.store.kategoriProduk.index', compact('datas'));
    }

    public function formAdd()
    {
        return view('admin.store.kategoriProduk.formAdd');
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'nama'=>'required'
        ]);

        $newData = new KategoriProduk();
        $newData->id_unit = Session::get('id_unit');
        $newData->jenis = $request->get('jenis');
        $newData->nama = $request->get('nama');
        $newData->save();

        return redirect('/admin/kategori-produk')->with('success', 'Distributor berhasil ditambahkan');
    }

    public function formEdit($id)
    {
        $data = KategoriProduk::find($id);

        return view('admin.store.kategoriProduk.formEdit', compact('data'));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'nama'=>'required'
        ]);

        $oldData = KategoriProduk::find($id);
        $oldData->jenis = $request->get('jenis');
        $oldData->nama = $request->get('nama');

        $oldData->update();

        return redirect('/admin/kategori-produk')->with('success', 'Distributor berhasil diperbaharui');
    }

    public function aktifasi(Request $request, $id)
    {
        $status = $request->get('is_aktif');
        $aktifasi = DB::table('kategori_produk')
            ->where('id', '=', $id)
            ->update(["is_aktif" => "$status"]);

        $modofiedData = DB::table('kategori_produk')
                    ->select('nama')
                    ->where('id', '=', $id)
                    ->first();

        $def = "";
        if ($status == "Y") {
            $def = "diaktifkan";
        } else {
            $def = "dinonaktifkan";
        }

        return redirect('/admin/kategori-produk')->with('success', "Kategori produk $modofiedData->nama berhasil $def.");
    }

    public function delete(Request $request, $id)
    {
        $data = KategoriProduk::find($id);
        $data->delete();

        return redirect('/admin/kategori-produk')->with('success', 'Distributor berhasil dihapus');
    }
}
