<?php

namespace App\Http\Controllers;

use App\TransaksiByr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class CustDashboardController extends Controller
{
    public function index()
    {
        $year = date('Y');
        $month = date('m');
        $idCustomer = Session::get('id_customer');

        $myProfile = DB::table('customer')
                ->leftJoin('tb_ro_cities', 'customer.id_kota', '=',  'tb_ro_cities.city_id')
                ->select('customer.nama', 'tb_ro_cities.city_name')
                ->where('id', '=', $idCustomer)
                ->first();

        $total = DB::table('transaksi')
                ->where('id_customer', '=', $idCustomer)
                ->where('stts_batal', '=', 0)
                ->where('stts_umum', '!=', 0)
                ->selectRaw('sum(ttl_transaksi) as ttl_omset, count(*) as ttl_transaksi')
                ->first();

        $omsetThisMonth = DB::table('transaksi')
                ->where('id_customer', '=', $idCustomer)
                ->where('stts_batal', '=', 0)
                ->where('stts_umum', '!=', 0)
                ->whereYear('tgl_transaksi', '=', $year)
                ->whereMonth('tgl_transaksi', '=', $month)
                ->sum('ttl_transaksi');

        // $listOrder = DB::table('transaksi')
        //     ->leftJoin('stts_transaksi', 'transaksi.stts_umum', '=', 'stts_transaksi.kode')
        //     ->select( 'transaksi.no_invoice', 'transaksi.is_dropshipper', 'transaksi.tgl_transaksi',
        //                 'transaksi.ttl_transaksi', 'stts_transaksi.nama as status', 'transaksi.request_jtr', 'transaksi.konfirmasi_jtr')
        //     ->where('transaksi.id_customer', '=', $idCustomer)
        //     ->orderBy('transaksi.no_invoice', 'DESC')
        //     ->limit(5)
        //     ->get();

        $listOrder = TransaksiByr::where('id_customer', '=', $idCustomer)
            ->orderBy('tgl_transaksi', 'DESC')
            ->limit(5)
            ->get();
        
        return view('customer.dashboard.index', \compact('myProfile', 'total', 'omsetThisMonth', 'listOrder'));
    }
}
