<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Database\Eloquent;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use DataTables;

use App\KategoriProduk;
use App\Produk;
use App\FotoProduk;

class ProdukController extends Controller
{
    public function index()
    {
        return view('admin.store.produk.index');
    }

    public function getDataJson()
    {
        $datas = DB::table('produk as a')
                ->leftJoin('kategori_produk as b', 'a.kategori_produk', '=', 'b.id')
                ->select('a.*', 'b.nama as nm_kategori')
                ->where('a.id_unit', Session::get('id_unit'));

        return DataTables::of($datas)
            ->editColumn('harga_jual' , function($datas){
                $result = "Rp " . number_format($datas->harga_jual,2,',','.');
                return $result;
            })
            ->editColumn('stok' , function($datas){
                $result = $datas->stok . " pcs";
                return $result;
            })
            ->addColumn('link' , function($datas){
                $links = '<a href="' . url('/admin/produk/form-edit', $datas->id) . '" class="btn btn-default btn-xs"><i class="fa fa-edit"></i> Edit</a> ';
                return $links;
            })
            ->addColumn('status' , function($datas){
                if ($datas->is_aktif == 'Y') {
                    $label = "<label class='badge badge-success'>Aktif</label>";
                } else {
                    $label = "<label class='badge badge-danger'>Nonaktif</label>";
                }
                return $label;
            })
            ->rawColumns(['link', 'status'])
            ->toJson();
    }

    public function formAdd()
    {
        $dtKategoriProduk = KategoriProduk::where("is_aktif", "Y")->get();
        return view('admin.store.produk.formAdd', compact('dtKategoriProduk'));
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'kode'=>'required',
            'nama'=>'required',
            'deskripsi'=>'required',
            'kategori_produk'=>'required',
            'berat'=>'required',
            'thumbnail'=>'required',
            'harga_modal'=>'required',
            'harga_jual'=>'required',
        ]);

        $newData = new Produk();
        $newData->id_unit = Session::get('id_unit');
        $newData->kode = $request->get('kode');
        $newData->nama = $request->get('nama');
        $newData->deskripsi = $request->get('deskripsi');
        $newData->kategori_produk = $request->get('kategori_produk');
        $newData->gender = $request->get('gender');
        $newData->berat = $request->get('berat');
        $newData->stok = $request->get('stok');
        $newData->harga_modal = $request->get('harga_modal');
        $newData->harga_jual = $request->get('harga_jual');
        $newData->save();

        // upload thumbnail
        if($request->hasFile('thumbnail'))
        {
            $file = $request->file('thumbnail');
            // rename filename
            $name = "thumbnail-$newData->id." . $file->getClientOriginalExtension();

            // upload file
            $file->move('foto-produk', $name);

            // record db
            $data = DB::table('produk')
                ->where('id', '=', $newData->id)
                ->update(["thumbnail" => $name]);
        }

        // upload foto pendukung
        if($request->hasFile('nama_file'))
        {
            $no = 1;
            foreach ($request->file('nama_file') as $foto) {
                // rename filename
                $newNameFotos = "foto-pendukung-$newData->id-" . $no++ . "." . $foto->getClientOriginalExtension();

                // upload file
                $foto->move('foto-produk', $newNameFotos);

                $newFoto = new FotoProduk();
                $newFoto->id_produk = $newData->id;
                $newFoto->nama_file = $newNameFotos;
                $newFoto->save();
            }

        }

        return redirect('/admin/produk')->with('success', 'Distributor berhasil ditambahkan');
    }

    public function formEdit($id)
    {
        $data = Produk::find($id);
        $dtFoto = FotoProduk::where("id_produk", "=", $id)->get();
        $dtKategoriProduk = KategoriProduk::where("is_aktif", "Y")->get();

        return view('admin.store.produk.formEdit', compact('data', 'dtFoto', 'dtKategoriProduk'));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'nama'=>'required',
            'deskripsi'=>'required',
            'kategori_produk'=>'required',
            'berat'=>'required',
            'harga_modal'=>'required',
            'harga_jual'=>'required',
            'is_aktif'=>'required',
        ]);

        $oldData = Produk::find($id);
        $oldData->kode = $request->get('kode');
        $oldData->nama = $request->get('nama');
        $oldData->deskripsi = $request->get('deskripsi');
        $oldData->kategori_produk = $request->get('kategori_produk');
        $oldData->gender = $request->get('gender');
        $oldData->berat = $request->get('berat');
        $oldData->stok = $request->get('stok');
        $oldData->harga_modal = $request->get('harga_modal');
        $oldData->harga_jual = $request->get('harga_jual');
        $oldData->is_aktif = $request->get('is_aktif');
        $oldData->update();

        // upload thumbnail
        if($request->hasFile('thumbnail'))
        {
            $file = $request->file('thumbnail');
            // rename filename
            $name = "thumbnail-$oldData->id." . $file->getClientOriginalExtension();

            // upload file
            $file->move('foto-produk', $name);

            // record db
            $data = DB::table('produk')
                ->where('id', '=', $oldData->id)
                ->update(["thumbnail" => $name]);
        }

        // upload foto pendukung
        if($request->hasFile('nama_file'))
        {
            $no = 1;
            foreach ($request->file('nama_file') as $foto) {
                // rename filename
                $newNameFotos = "foto-pendukung-$oldData->id-" . $no++ . "." . $foto->getClientOriginalExtension();

                // upload file
                $foto->move('foto-produk', $newNameFotos);

                $newFoto = new FotoProduk();
                $newFoto->id_produk = $id;
                $newFoto->nama_file = $newNameFotos;
                $newFoto->save();
            }

        }

        return redirect('/admin/produk/form-edit/' . $id)->with('success', 'Distributor berhasil diperbaharui');
    }

    public function deleteThumbnail($id, $thumbnail)
    {
        // update thumbnail set ""
        $data = DB::table('produk')
            ->where('id', '=', $id)
            ->update(["thumbnail" => ""]);

        // remove file
        File::delete('foto-produk/'.  $thumbnail);

        return redirect("/admin/produk/form-edit/$id")->with('success', 'Thumbnail Produk berhasil dihapus');
    }

    public function deleteFotoPendukung($idProduk, $idFoto, $namaFile)
    {
        // delete from db
        $data = FotoProduk::find($idFoto);
        $data->delete();

        // remove file
        File::delete('foto-produk/'.  $namaFile);

        return redirect("/admin/produk/form-edit/$idProduk")->with('success', 'Thumbnail Produk berhasil dihapus');
    }

    public function detail($id)
    {
        $data = Produk::find($id);
        $dtFoto = FotoProduk::where("kode_produk", "=", $data->kode)->get();

        return view('admin.store.produk.detail', compact('data', 'dtFoto'));
    }

    // public function delete(Request $request, $id)
    // {
    //     $data = KategoriProduk::find($id);
    //     $data->delete();

    //     return redirect('/admin/kategori-produk')->with('success', 'Distributor berhasil dihapus');
    // }
}
