<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Hash;

use App\Customer;
use App\SysRefUser;

class CustomerController extends Controller
{
    public function index()
    {
        $datas = DB::table('customer')
            ->leftJoin('tb_ro_cities', 'customer.id_kota', '=',  'tb_ro_cities.city_id')
            ->select('customer.id', 'customer.nama', 'customer.is_aktif', 'customer.no_hp', 'tb_ro_cities.city_name')
            ->orderBy('customer.nama', 'ASC')
            ->paginate(10);
        return view('admin.store.customer.index', compact('datas'));
    }

    public function formAdd()
    {
        $dtProvinsi = DB::table('tb_ro_provinces')
            ->orderBy('province_name', 'ASC')
            ->get();
        return view('admin.store.customer.formAdd', compact('dtProvinsi'));
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'nama'=>'required',
            'no_hp'=>'required',
            'alamat'=>'required',
            'id_kecamatan'=>'required',
            'id_kota'=>'required',
            'id_provinsi'=>'required'
        ]);

        $newData = new Customer();
        $newData->nama = $request->get('nama');
        $newData->no_hp = $request->get('no_hp');
        $newData->alamat = $request->get('alamat');
        $newData->id_kecamatan = $request->get('id_kecamatan');
        $newData->id_kota = $request->get('id_kota');
        $newData->id_provinsi = $request->get('id_provinsi');
        $newData->save();

        // add user login
        $data = $request->all();
        $data['id_customer'] = $newData->id;
        $this->saveDataUser($data);

        return redirect('/admin/customer')->with('success', 'Distributor berhasil ditambahkan');
    }

    public function saveDataUser($data)
    {
        $newData                    = new SysRefUser();
        $newData->nama              = $data['nama'];
        $newData->username          = $data['username'];
        $newData->password          = Hash::make($data['password']);
        $newData->id_usergroup      = ',3,';
        $newData->id_distributor    = $data['id_distributor'];
        $newData->is_aktif          = 'Y';
        $newData->save();
    }

    public function formEdit($id)
    {
        $data = Customer::where('id', '=', $id)->first();

        $dtProvinsi = DB::table('tb_ro_provinces')
            ->orderBy('province_name', 'ASC')
            ->get();

        $dtKota = DB::table('tb_ro_cities')
            ->where('province_id', '=', $data->id_provinsi)
            ->orderBy('city_name', 'ASC')
            ->get();

        $dtKecamatan = DB::table('tb_ro_subdistricts')
            ->where('city_id', '=', $data->id_kota)
            ->orderBy('subdistrict_name', 'ASC')
            ->get();

        return view('admin.store.customer.formEdit', compact('data', 'dtProvinsi', 'dtKota', 'dtKecamatan'));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'nama'=>'required',
            'no_hp'=>'required',
            'alamat'=>'required',
            'id_kecamatan'=>'required',
            'id_kota'=>'required',
            'id_provinsi'=>'required'
        ]);

        $oldData = Customer::find($id);
        $oldData->nama = $request->get('nama');
        $oldData->no_hp = $request->get('no_hp');
        $oldData->alamat = $request->get('alamat');
        $oldData->id_kecamatan = $request->get('id_kecamatan');
        $oldData->id_kota = $request->get('id_kota');
        $oldData->id_provinsi = $request->get('id_provinsi');
        $oldData->update();

        return redirect('/admin/customer')->with('success', 'Distributor berhasil diperbaharui');
    }

    public function aktifasi(Request $request, $id)
    {
        $status = $request->get('is_aktif');
        $aktifasi = DB::table('customer')
            ->where('id', '=', $id)
            ->update(["is_aktif" => "$status"]);

        $modofiedData = DB::table('customer')
                    ->select('nama')
                    ->where('id', '=', $id)
                    ->first();

        $def = "";
        if ($status == "Y") {
            $def = "diaktifkan";
        } else {
            $def = "dinonaktifkan";
        }

        // call aktivation user function
        $this->aktifasiUser($id, $status);

        return redirect('/admin/customer')->with('success', "Distributor $modofiedData->nama berhasil $def.");
    }

    public function aktifasiUser($id, $status)
    {
        $aktifasi = DB::table('sys_ref_user')
            ->where('id_customer', '=', $id)
            ->update(["is_aktif" => "$status"]);
    }

    public function delete(Request $request, $id)
    {
        $data = MbcDistributor::find($id);
        $data->delete();

        return redirect('/admin/customer')->with('success', 'Distributor berhasil dihapus');
    }
}
