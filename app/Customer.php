<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = "customer";
    protected $fiilable = [
        "nama",
        "no_hp",
        "alamat",
        "id_provinsi",
        "id_kota",
        "id_kecamatan",
        "is_aktif",
    ];
}
