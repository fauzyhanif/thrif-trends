<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GnrtInvoice extends Model
{
    protected $table = "gnrt_invoice";
    protected $fiilable = [
        "bulan",
        "urut"
    ];
}
