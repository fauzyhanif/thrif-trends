<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerificationMail extends Mailable
{
    use Queueable, SerializesModels;
    private $title;
    private $email;
    private $token;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($title, $email, $token)
    {
        $this->title = $title;
        $this->email = $email;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $compacts = ['title' => $this->title, 'email' => $this->email, 'token' => $this->token];
        return $this->view('auth.email', $compacts);
    }
}
