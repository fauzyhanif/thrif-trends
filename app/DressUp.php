<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DressUp extends Model
{
    protected $table = "dress_ups";
    protected $fillable = [
        "id_produk",
        "jenis_produk",
        "id_customer",
        "created_at",
        "updated_at",
    ];

    public function _produk()
    {
        return $this->belongsTo(Produk::class, 'id_produk');    
    }
}
