<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriProduk extends Model
{
    protected $table = "kategori_produk";
    protected $fiilable = [
        "id",
        "nama"
    ];
}
