<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keranjang extends Model
{
    protected $table = "keranjang";
    protected $fiilable = [
        "id",
        "id_unit",
        "id_customer",
        "kode_produk",
        "qty",
        "harga",
        "jumlah",
        "is_dress_up",
        "created_at",
        "updated_at",
    ];
}
