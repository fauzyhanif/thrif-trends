<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FotoProduk extends Model
{
    protected $table = "foto_produk";
    protected $fiilable = [
        "id",
        "id_produk",
        "nama_file"
    ];
}
