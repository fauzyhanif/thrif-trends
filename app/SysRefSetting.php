<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysRefSetting extends Model
{
    protected $table = "sys_ref_setting";
    protected $primaryKey = "id_unit";
    protected $fiilable = [
        "id_unit",
        "nama",
        "logo",
        "no_hp_1",
        "no_hp_2",
        "email",
        "alamat",
        "kodepos",
        "id_kecamatan",
        "id_kota",
        "id_provinsi",
    ];

    public function _city()
    {
        return $this->belongsTo(City::class, 'id_kota', 'city_id');    
    }
}
