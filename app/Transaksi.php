<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = "transaksi";
    protected $fillable = [
        "id",
        "id_unit",
        "id_transaksi_byr",
        "tgl_transaksi",
        "no_invoice",
        "id_customer",
        "nama",
        "no_hp",
        "alamat",
        "id_kecamatan",
        "id_kota",
        "id_provinsi",
        "is_dropshipper",
        "dropship_nama",
        "dropship_no_hp",
        "dropship_alamat",
        "dropship_id_kecamatan",
        "dropship_id_kota",
        "dropship_id_provinsi",
        "logistik_name",
        "logistik_service",
        "logistik_day",
        "request_jtr",
        "konfirmasi_jtr",
        "ttl_qty",
        "ttl_ongkir",
        "ttl_berat",
        "ttl_belanja",
        "ttl_transaksi",
        "stts_umum",
        "stts_bayar",
        "stts_batal",
        "catatan",
        "subtraction_stock",
        "resi"
    ];

    public function _unit()
    {
        return $this->belongsTo(SysRefSetting::class, 'id_unit', 'id_unit');    
    }

    public function _details()
    {
        return $this->hasMany(TransaksiDtl::class, 'no_invoice', 'no_invoice');
    }
}
