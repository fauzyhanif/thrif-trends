<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = "tb_ro_cities";
    protected $fiilable = [
        "city_id",
        "province_id",
        "city_name",
        "postal_code"
    ];
}
