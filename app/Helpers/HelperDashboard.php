<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Session;

class HelperDashboard {
    public static function getTotalTransaksi()
    {
        $total = DB::table('transaksi')
                ->selectRaw('sum(ttl_transaksi) as total')
                ->where('stts_umum', '!=', '0')
                ->where('id_unit', Session::get('id_unit'))
                ->first();
        return $total->total;
    }

    public static function getTotalTransaksiToday()
    {
        $total = DB::table('transaksi')
                ->selectRaw('sum(ttl_transaksi) as total')
                ->where('stts_umum', '!=', '0')
                ->whereDate('tgl_transaksi', Carbon::today())
                ->where('id_unit', Session::get('id_unit'))
                ->first();
        return $total->total;
    }

    public static function getTotalTransaksiThisMonth()
    {
        $firstDay = date('Y-m-01');
        $lastDay = date('Y-m-t');
        $thisMonth = date('Y-m');

        $total = DB::table('transaksi')
                ->selectRaw('sum(ttl_transaksi) as total')
                ->where('stts_umum', '!=', '0')
                ->where('tgl_transaksi', "like", "%$thisMonth%")
                ->where('id_unit', Session::get('id_unit'))
                // ->whereBetween('tgl_transaksi', [$firstDay, $lastDay])
                ->first();
        return $total->total;
    }

    public static function getTotalDistributor()
    {
        $total = DB::table('customer')
                ->selectRaw('count(*) as total')
                ->first();
        return $total->total;
    }

    public static function getTotalKategoriProduk()
    {
        $total = DB::table('kategori_produk')
                ->selectRaw('count(*) as total')
                ->where('id_unit', Session::get('id_unit'))
                ->first();
        return $total->total;
    }

    public static function getTotalProduk()
    {
        $total = DB::table('produk')
                ->selectRaw('count(*) as total')
                ->where('id_unit', Session::get('id_unit'))
                ->first();
        return $total->total;
    }

    public static function getOmsetDistributor()
    {
        $datas = DB::table('transaksi')
                ->leftJoin('customer', 'transaksi.id_customer', '=', 'customer.id')
                ->leftJoin('tb_ro_cities', 'customer.id_kota', '=',  'tb_ro_cities.city_id')
                ->select('customer.nama', 'tb_ro_cities.city_name',
                        DB::raw('SUM(transaksi.ttl_belanja) as jml_omset,
                        SUM(transaksi.ttl_qty) as jml_qty
                        '))
                ->where('transaksi.stts_umum', '!=', '0')
                ->where('transaksi.id_unit', Session::get('id_unit'))
                ->orderBy('jml_omset', 'DESC')
                ->groupBy('transaksi.id_customer', 'customer.nama', 'tb_ro_cities.city_name')
                ->limit(5)
                ->get();

        return $datas;
    }

    public static function getLastTrx()
    {
        $datas = DB::table('transaksi as a')
                ->leftJoin('stts_transaksi as b', 'a.stts_umum', '=', 'b.kode')
                ->select('a.no_invoice', 'a.is_dropshipper', 'a.nama', 'a.ttl_transaksi', 'b.nama as status')
                ->where('a.id_unit', Session::get('id_unit'))
                ->orderBy('no_invoice', 'DESC')
                ->limit(5)
                ->get();

        return $datas;
    }
}
?>
