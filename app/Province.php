<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = "tb_ro_provinces";
    protected $fiilable = [
        "province_id",
        "province_name"
    ];
}
