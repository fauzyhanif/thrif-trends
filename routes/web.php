<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
///////////////////////////////////////////
//////////////* ADMIN *//////////////
///////////////////////////////////////////

# home page
Route::get('/', 'DashboardController@index');
Route::get('', 'DashboardController@index');

# Dashboard
Route::get('/dashboard', 'DashboardController@index')->middleware('admin');

# Setting Web
Route::get('/admin/setting-web', 'SettingWebController@index')->middleware('admin');
Route::get('/admin/setting-web/form-edit', 'SettingWebController@formEdit')->middleware('admin');
Route::post('/admin/setting-web/edit/{id}', 'SettingWebController@edit')->middleware('admin');

# Helper
Route::get('/helpers/cari-kota/{id}', 'HelpersController@cariKota');
Route::get('/helpers/cari-kecamatan/{id}', 'HelpersController@cariKecamatan');
Route::get('/helpers/cari-desa/{id}', 'HelpersController@cariDesa');

# Auth page
Route::get('/auth', 'AuthController@index');
Route::post('/doAuth', 'AuthController@doAuth');
Route::get('/auth/pilih-usergroup', 'AuthController@pilihUsergroup');
Route::get('/set-usergroup/{id}', 'AuthController@setUsergroup');
Route::get('/logout', 'AuthController@logout');
Route::get('/access-denied', 'AuthController@accessDenied');
Route::get('/register', 'AuthController@registerPage');
Route::post('/register', 'AuthController@doRegister');
Route::get('/token-verification', 'AuthController@tokenVerification');
Route::get('/resend-email-verification', 'AuthController@resendEmailVerification');
Route::get('/email-verification-page', 'AuthController@emailVerificationPage');

# Pilih Sesi
Route::get('/pilih-sesi', 'SessionController@pilihSesiTahunAkd');
Route::get('/set-sesi-tahun-akd/{thnakd}/{gelombang}', 'SessionController@setSesiTahunAkd');

# User page
Route::get('/user', 'UserController@index')->middleware('admin');
Route::get('/user-json', 'UserController@json')->middleware('admin');
Route::get('/user/form-add', 'UserController@formAdd')->middleware('admin');
Route::post('/user/add-new', 'UserController@addNew')->middleware('admin');
Route::get('/user/form-edit/{id}', 'UserController@formEdit')->middleware('admin');
Route::post('/user/edit/{id}', 'UserController@edit')->middleware('admin');
Route::get('/user/delete/{id}', 'UserController@delete')->middleware('admin');
Route::get('/user/reset-password/{id}', 'UserController@resetPassword')->middleware('admin');

# Usergroup page
Route::get('/usergroup', 'UsergroupController@index')->middleware('admin');
Route::get('/usergroup/form-add', 'UsergroupController@formAdd')->middleware('admin');
Route::post('/usergroup/add-new', 'UsergroupController@addNew')->middleware('admin');
Route::get('/usergroup/form-edit/{id}', 'UsergroupController@formEdit')->middleware('admin');
Route::post('/usergroup/edit/{id}', 'UsergroupController@edit')->middleware('admin');
Route::get('/usergroup/delete/{id}', 'UsergroupController@delete')->middleware('admin');

# CRUD Distributor
Route::get('/admin/customer', 'CustomerController@index')->middleware('admin');
Route::get('/admin/customer/form-add', 'CustomerController@formAdd')->middleware('admin');
Route::post('/admin/customer/add-new', 'CustomerController@addNew')->middleware('admin');
Route::get('/admin/customer/form-edit/{id}', 'CustomerController@formEdit')->middleware('admin');
Route::post('/admin/customer/edit/{id}', 'CustomerController@edit')->middleware('admin');
Route::post('/admin/customer/delete/{id}', 'CustomerController@delete')->middleware('admin');
Route::post('/admin/customer/aktifasi/{id}', 'CustomerController@aktifasi')->middleware('admin');

# CRUD Kategori Produk
Route::get('/admin/kategori-produk', 'KategoriProdukController@index')->middleware('admin');
Route::get('/admin/kategori-produk/form-add', 'KategoriProdukController@formAdd')->middleware('admin');
Route::post('/admin/kategori-produk/add-new', 'KategoriProdukController@addNew')->middleware('admin');
Route::get('/admin/kategori-produk/form-edit/{id}', 'KategoriProdukController@formEdit')->middleware('admin');
Route::post('/admin/kategori-produk/edit/{id}', 'KategoriProdukController@edit')->middleware('admin');
Route::post('/admin/kategori-produk/delete/{id}', 'KategoriProdukController@delete')->middleware('admin');
Route::post('/admin/kategori-produk/aktifasi/{id}', 'KategoriProdukController@aktifasi')->middleware('admin');

# CRUD produk
Route::get('/admin/produk', 'ProdukController@index')->middleware('admin');
Route::get('/admin/produk/detail/{id}', 'ProdukController@detail')->middleware('admin');
Route::get('/admin/data-produk-json', 'ProdukController@getDataJson')->middleware('admin');
Route::get('/admin/produk/form-add', 'ProdukController@formAdd')->middleware('admin');
Route::post('/admin/produk/add-new', 'ProdukController@addNew')->middleware('admin');
Route::get('/admin/produk/form-edit/{id}', 'ProdukController@formEdit')->middleware('admin');
Route::post('/admin/produk/edit/{id}', 'ProdukController@edit')->middleware('admin');
Route::post('/admin/produk/delete/{id}', 'ProdukController@delete')->middleware('admin');
Route::get('/admin/produk/delete-thumbnail/{id}/{thumbnail}', 'ProdukController@deleteThumbnail')->middleware('admin');
Route::get('/admin/produk/delete-foto-pendukung/{idProduk}/{idFoto}/{namaFile}', 'ProdukController@deleteFotoPendukung')->middleware('admin');

# Transaksi
Route::get('/admin/list-transaksi/{idStatus?}', 'AdmTransaksiController@index')->middleware('admin');
Route::get('/admin/get-json-transaksi/{idStatus?}', 'AdmTransaksiController@getTransactionByMonth')->middleware('admin');
Route::get('/admin/transaksi/detail/{invoice}', 'AdmTransaksiController@detail')->middleware('admin');
Route::post('/admin/transaksi/ubah-status/{invoiceId}', 'AdmTransaksiController@changeStatusTransaction')->middleware('admin');
Route::get('/admin/transaksi/print-invoice/{invoice}', 'AdmTransaksiController@printInvoice')->middleware('admin');
Route::post('/admin/transaksi/input-ongkir-jtr/{invoice}', 'AdmTransaksiController@inputOngkirJtr')->middleware('admin');
Route::get('/admin/transaksi/print-invoice-banyak/{idStatus?}', 'AdmTransaksiController@printInvoiceBanyak')->middleware('admin');
Route::get('/admin/transaksi/get-json-for-print/{idStatus?}', 'AdmTransaksiController@getTransactionForPrint')->middleware('admin');
Route::post('/admin/transaksi/do-print-invoice-banyak', 'AdmTransaksiController@doPrintInvoiceBanyak')->middleware('admin');

# Laporan kategori produk
Route::match(['get', 'post'],'/admin/laporan/laba-kategori-produk', 'AdmLapKategoriProdukController@index')->middleware('admin');
Route::get('/admin/laporan/laba-kategori-produk/detail/{id}/{first}/{last}', 'AdmLapKategoriProdukController@detail')->middleware('admin');

# Laporan produk
Route::match(['get', 'post'],'/admin/laporan/laba-produk', 'AdmLapProdukController@index')->middleware('admin');

# Laporan Customer
Route::match(['get', 'post'],'/admin/laporan/customer', 'AdmLapCustomerController@index')->middleware('admin');


///////////////////////////////////////////
//////////////* DISTRIBUTOR *//////////////
///////////////////////////////////////////

# check session & errors
Route::get('/check-session-with-js', 'AuthController@CheckSessionWithJs');
Route::get('/login-ulang', 'AuthController@loginUlang');

# Dashboard
Route::get('/customer/dashboard', 'CustDashboardController@index')->middleware('dist');

# Profile
Route::get('/customer/profile', 'CustProfileController@index')->middleware('dist');
Route::get('/customer/profile/form-edit', 'CustProfileController@formEdit')->middleware('dist');
Route::post('/customer/profile/edit/{id}', 'CustProfileController@edit')->middleware('dist');
Route::get('/customer/form-ubah-password', 'CustProfileController@formUbahPassword')->middleware('dist');
Route::post('/customer/ubah-password', 'CustProfileController@ubahPassword')->middleware('dist');

# Produk
Route::get('/customer/produk/{kategori?}', 'CustProdukController@index')->middleware('dist');
Route::get('/customer/produk/detail/{id}', 'CustProdukController@detail')->middleware('dist');

# Search
Route::match(['get', 'post'],'/customer/produk-search', 'CustProdukController@search')->middleware('dist');

# Keranjang
Route::get('/customer/keranjang', 'CustKeranjangController@index')->middleware('dist');
Route::post('/customer/add-to-cart', 'CustKeranjangController@addToCart')->middleware('dist');
Route::get('/customer/reload-header-count-cart', 'CustKeranjangController@ReloadHeaderCountCart')->middleware('dist');
Route::get('/customer/reload-header-list-cart', 'CustKeranjangController@ReloadHeaderListCart')->middleware('dist');
Route::post('/customer/keranjang/ubah-item', 'CustKeranjangController@updateDataItem')->middleware('dist');
Route::post('/customer/keranjang/hapus-item', 'CustKeranjangController@removeDataItem')->middleware('dist');

# Checkout
Route::get('/customer/checkout', 'CustCheckoutController@index')->middleware('dist');
Route::post('/customer/checkout/buat-pesanan', 'CustCheckoutController@createOrder')->middleware('dist');
Route::get('/customer/pembayaran/{invoice}', 'CustCheckoutController@pembayaran')->middleware('dist');
Route::post('/customer/checkout/get-cost-dropshipper', 'CustCheckoutController@getCostDropshipper')->middleware('dist');
Route::post('/customer/checkout/request-jtr', 'CustCheckoutController@requestJtr')->middleware('dist');
Route::get('/customer/checkout/waiting-request-jtr/{invoice}', 'CustCheckoutController@waitingRequestJtr')->middleware('dist');

# Pesanan
Route::get('/customer/pesanan/{sttsUmum?}', 'CustPesananController@index')->middleware('dist');
Route::get('/customer/pesanan/detail/{invoice}', 'CustPesananController@detail')->middleware('dist');
Route::post('/customer/pesanan/konfirmasi-sampai', 'CustPesananController@konfirmasiSampai')->middleware('dist');

Route::get('/customer/dress-up', 'CustDressUpController@index')->middleware('dist');
Route::post('/customer/dress-up/create', 'CustDressUpController@create')->middleware('dist');
Route::get('/customer/dress-up/get', 'CustDressUpController@get')->middleware('dist');